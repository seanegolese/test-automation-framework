package Utility;


import org.openqa.selenium.*;

public class FindElement extends SeleniumBaseClass {
	
    public static String ElementXpath(String value) {
    	 WebElement FindElement = driver.findElement(By.xpath(value));
 		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", FindElement);
 		driver.findElement(By.xpath(value)).click();

       return value;
    }
    
    public static String ElementID(String value) {
   	 WebElement FindElement = driver.findElement(By.id(value));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", FindElement);
		driver.findElement(By.id(value)).click();

      return value;
   }
    
    public static String ElementName(String value){
      	 WebElement FindElement = driver.findElement(By.name(value));
   		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", FindElement);
   		driver.findElement(By.name(value)).click();

         return value;
      }
    
    
    
    public WebElement positionElementCenterBeforeClick(String xPATH)
    {
    	WebElement element = driver.findElement(By.xpath(xPATH));

    	String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
    	                                            + "var elementTop = arguments[0].getBoundingClientRect().top;"
    	                                            + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

    	((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);
    	
    	return element;
    }
   
}