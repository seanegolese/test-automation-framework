package Utility;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Login;

public class SeleniumBaseClass {

	protected static WebDriver driver;
	protected String results = "";
	protected String runId = "";
	protected String appurl = "";
	protected String ExcelLocation = "";
	public String caseId = "";
	protected boolean saveScreenshots = true;
	protected static Statement statement;
	protected Connection connection;
	protected static ResultSet rs;

	public static ExtentReports extent;
	public static ExtentTest logger;

	// Common functions under Functions package
	public Login login = new Login();

	public String environmnet = "";

	@BeforeClass(alwaysRun = true)
	@Parameters({ "run-id-param", "save-screenshots", "databaseURL", "user", "password", "appurl", "ExcelLocation" })
	public void setUp(String _runId, String _saveScreenshots, String _databaseURL, String _user, String _password,
			String _appurl, String _ExcelLocation) throws MalformedURLException {

		ChromeOptions options = new ChromeOptions();

		options.addArguments("--start-maximized");
		System.out.println("Operating System:"+System.getProperty("os.name"));
		System.setProperty("webdriver.chrome.driver", "WebDrivers/chromedriver.exe");
		System.out.println("******************************************");
		System.out.println("Launching Chrome browser");
		try {			
			driver = new ChromeDriver(options);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		appurl = _appurl;
		runId = _runId;
		ExcelLocation = _ExcelLocation;
		environmnet = _ExcelLocation;

		if (_saveScreenshots.compareTo("true") == 0)
			saveScreenshots = true;

		String databaseURL = _databaseURL;
		String user = _user;
		String password = _password;

		connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Connecting to Database...");
			connection = DriverManager.getConnection(databaseURL, user, password);
			if (connection != null) {
				System.out.println("Connected to the Database...");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}

	}

	// Used to get case ID from Child Class
	public String getId() {

		return caseId;
	}

	public void setId(String caseId) {

		this.caseId = caseId;
	}

	@AfterMethod
	@Parameters({ "posttotestRail" })
	public void tearDown(ITestResult result, String posttotestRail) throws MalformedURLException, IOException {
		APIClient client;
		Map data;

		DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
		Date dateobj = new Date();
		String TransactionDate = ((df.format(dateobj)));

		client = new APIClient("https://hellogroup.testrail.com");
		client.setUser("apitest@testing.co.za");
		client.setPassword("formula.1");

		data = new HashMap();
		data.put("case_id", getId());

		String parameters = "";

		int i = 0;
		for (Object o : result.getParameters()) {
			parameters += "param" + i + ": " + o.toString() + "\n";
			i++;
		}

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		Throwable cause = result.getThrowable();
		if (null != cause && saveScreenshots) {

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				File file = new File(".");
				String filename = result.getTestClass() + TransactionDate;
				FileUtils.copyFile(scrFile,
						new File(file.getAbsolutePath() + "/target/surefire-reports/Report/imgs/" + filename + ".jpg"));
				ReportListener.screenshots.add(filename);

			} catch (IOException e) {
				e.printStackTrace();
			}

			cause.printStackTrace(pw);
			String[] lines = sw.getBuffer().toString().split("\\n");
			results += "\nReason for failure:\n" + lines[0];
		}

		if (result.getStatus() == ITestResult.FAILURE) {
			System.out.println("Test failed");

			data.put("status_id", new Integer(5));
			data.put("comment", "This test failed\n" + parameters + "\n" + results);
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			System.out.println("Test succeeded");

			data.put("status_id", new Integer(1));
			data.put("comment", "This test succeeded\n" + parameters + "\n" + results);
		}

		System.out.println("Post to Test Rail : " + posttotestRail + "");

		if (posttotestRail.equalsIgnoreCase("Yes")) {
			System.out.println(results);
			System.out.println("Post result to testrail");
			try {
				JSONObject r = (JSONObject) client.sendPost("add_result_for_case/" + runId + "/" + getId(), data);
			} catch (APIException e) {
				driver.quit();
				e.printStackTrace();
			}
		}
	}

	
	
	@BeforeSuite
	public void startReport() {

		String hostname = "Unknown";
		try {
			InetAddress addr;
			addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch (UnknownHostException ex) {
			System.out.println("Hostname can not be resolved....");
		}

		extent = new ExtentReports(System.getProperty("user.dir") + "\\test-output\\CRM_Automation_Report.html", true);
		extent.addSystemInfo("Environment", "Quality Assurance").addSystemInfo("Host Name", hostname)
				.addSystemInfo("Environment", "QA").addSystemInfo("User Name", System.getProperty("user.name"))
				.addSystemInfo("Project", "CRM");
		extent.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));

	}

	
	// The following method take screenshot and return the path to that screenshot
	private static String getScreenShot(WebDriver driver, String fileName) throws IOException {

		DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
		Date dateobj = new Date();
		String TransactionDate = ((df.format(dateobj)));

		fileName = fileName + TransactionDate + ".png";
		String directory = "./test-output/img/" + fileName;

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(directory));

		String destination = directory;

		return destination;
	}
	
	

	@AfterMethod
	public void getResult(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String ImagePath = logger.addScreenCapture(getScreenShot(driver, result.getName()));
			logger.log(LogStatus.FAIL, "Test case failed is " + result.getName(), ImagePath);
			logger.log(LogStatus.FAIL, result.getName() + " failed due to:\n", result.getThrowable());
		}

		if (result.getStatus() == ITestResult.SKIP) {
			logger.log(LogStatus.SKIP, "Test case skipped is " + result.getName(), result.getThrowable());
		}

	}
	
	@AfterSuite
	public static void closeReport() throws Exception {
		extent.flush();
		Report report = new Report();
		report.processReport();
	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		connection.close();
	}

}
