package Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;

import Utility.ReportListener.SMTPAuthenticator;

public class Report {

	public Report() {}
	
	private static final String SMTP_HOST_NAME = "mail.telestream.co.za";
	private static final String SMTP_AUTH_USER = "Jenkins";
	private static final String SMTP_AUTH_PWD = "OkIoMmdjlrvoaNwG1vSn";
	
	
	public String dummyMail = "dummy@hellogourp.co.za";
	public String to = "lesetja.seanego@hellogroup.co.za";
	public String from = "jenkins@hellogroup.co.za";
	public String subject = "CRM Automation Report ";
	public String body = "Hello Group Automation Framework CRM";

	public String email_to = "vukile.maila@hellogroup.co.za";
	public String email_cc = "mark.redelinghuys@hellogroup.co.za";
	public String email_cc2 = "vusi.monama@hellogroup.co.za";
	
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		
		public PasswordAuthentication getPasswordAuthentication() {
			String username = SMTP_AUTH_USER;
			String password = SMTP_AUTH_PWD;
			return new PasswordAuthentication(username, password);
		}
	}
	
	private void mail(String to, String from, String host, String path, String filename, String subject, String body) {

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", SMTP_HOST_NAME);
		props.put("mail.smtp.auth", "true");

		try {

			Authenticator auth = new SMTPAuthenticator();
			Session mailSession = Session.getDefaultInstance(props, auth);
			MimeMessage message = new MimeMessage(mailSession);
			Transport transport = mailSession.getTransport();
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			message.setSubject(subject);
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(body);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Attachment
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(path + filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);

			transport.connect();
			Transport.send(message);
			System.out.println("Report sent to " + to);
			transport.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	//Create folders for the report
	private static void buildRportDirectory()
	{
		System.out.println("Creating report directories");
		File main = new File("./TestDirectory/test-output/img");
	    boolean successful = main.mkdirs();
	    if (successful)
	    {
	    System.out.println("Report directiry created");
	    }
	    else{System.out.println("failed trying to create the directory");}
	}
		
	//Move files to the relevant folders in the report directory	
	private static void copyReportFilesToReportDirectory()
	{
		
		//move html report
		try
		{
		File source =new File("./test-output/CRM_Automation_Report.html");
		File destiantion =new File("./TestDirectory/CRM_Automation_Report.html");
		FileUtils.copyFile(source, destiantion);
		
		File source1 =new File("./test-output/img/");
		File destiantion1 =new File("./TestDirectory/test-output/img/");
		FileUtils.copyDirectory(source1, destiantion1);
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	private static void zipFolder(final Path sourceFolderPath, Path zipPath) throws Exception {

		final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));

		Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {

			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

				zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));

				Files.copy(file, zos);

				zos.closeEntry();

				return FileVisitResult.CONTINUE;

			}
		});

		zos.close();

	}
		
	private static void zipReportDirectory() 
	{
		//create folders	
		String source = System.getProperty("user.dir")+"/TestDirectory";
		String zimpedFolder = System.getProperty("user.dir") + "/Hello_Group_Automation_Report.zip";

		try {
			zipFolder(Paths.get(source), Paths.get(zimpedFolder));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	private void SendReport()
	{
		mail(dummyMail, from, "mail.hellogroup.co.za", System.getProperty("user.dir") + "\\",
				"Hello_Group_Automation_Report.zip", subject, body);
		mail(to, from, "mail.hellogroup.co.za", System.getProperty("user.dir") + "\\", "Hello_Group_Automation_Report.zip",
				subject, body);

		mail(email_to, from, "mail.hellogroup.co.za", System.getProperty("user.dir") + "\\",
				"Hello_Group_Automation_Report.zip", subject, body);
		mail(email_cc, from, "mail.hellogroup.co.za", System.getProperty("user.dir") + "\\",
				"Hello_Group_Automation_Report.zip", subject, body);
		mail(email_cc2, from, "mail.hellogroup.co.za", System.getProperty("user.dir") + "/",
				"Hello_Group_Automation_Report.zip", subject, body);

	}
	
	private void deleteReportDirectory()
	{
		File imgFile = new File(System.getProperty("user.dir") + "\\test-output\\img\\");
		ReportListener.deleteDir(imgFile);		
		//delete zipped report
		File zippedReport = new File( System.getProperty("user.dir") + "/Hello_Group_Automation_Report.zip");
		ReportListener.deleteDir(zippedReport);
	}
	
	public void processReport()
	{
		buildRportDirectory();
		copyReportFilesToReportDirectory();
		zipReportDirectory();
		SendReport();
		deleteReportDirectory();
	}
}
