package HelloGroup.CRM.Functions;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.POM.CRM_POM;
import Utility.SeleniumBaseClass;

public class Login extends CRM_POM {
	public void SignIn(WebDriver driver, String sUsername, String sPassword, String AppURL)
			throws InterruptedException {
		try {

			System.out.println("Signing In to CRM");
			driver.navigate().to(AppURL);
			String strPageTitle = driver.getTitle();
			System.out.println("Page title: - " + strPageTitle);

			// Look for username field before entering the credentials
			WebElement ClickTab = username(driver);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			username(driver).sendKeys(sUsername);
			password(driver).sendKeys(sPassword);
			loginButton(driver).click();

			String strPageURL = driver.getCurrentUrl();
			System.out.println("Page url: - " + strPageURL);

			results = "";
			results += "Actual: " + strPageURL + "\n";
			results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			if (driver.getCurrentUrl().equalsIgnoreCase("" + AppURL + "pages/main.php")) {
				Assert.assertTrue(true);
				SeleniumBaseClass.logger.log(LogStatus.PASS, "Login successful");
			} else {

				Assert.assertTrue(false);
				SeleniumBaseClass.logger.log(LogStatus.FAIL, "Login unsuccessful");
			}
		} catch (Exception ex) {
			SeleniumBaseClass.logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
		}
		Thread.sleep(2000);
	}

}
