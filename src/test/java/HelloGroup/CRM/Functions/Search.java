package HelloGroup.CRM.Functions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.POM.CRM_POM;
import Utility.SeleniumBaseClass;

public class Search extends CRM_POM {

	public SeleniumBaseClass seleniumBaseclass = new SeleniumBaseClass();

	public Search() {
	}

	public void hpId(WebDriver driver, String sHPID, String AppURL) throws InterruptedException {
		try
		{
		Thread.sleep(5000);
		System.out.println("Searching ID");	
		
		WebElement ClickTab = hPID_radioButton(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);		
		hPID_radioButton(driver).click();
		
		customerHdIdInputField(driver).clear();
		customerHdIdInputField(driver).sendKeys(sHPID);
		Thread.sleep(5000);
		searchCustomerButton(driver).click();
		String strPageURL = driver.getCurrentUrl();

		Assert.assertTrue(strPageURL.equalsIgnoreCase("" + AppURL + "pages/main.php"), "Page title doesn't match");
		Reporter.log("Searched for   " + sHPID + " on " + AppURL + " ");
		SeleniumBaseClass.logger.log(LogStatus.PASS,"HP ID Search");
		}
		catch(Exception ex)
		{
			SeleniumBaseClass.logger.log(LogStatus.FAIL,"HP ID search failed"+ex.getMessage());
		}
		
		
	}

	public void cellNumber(WebDriver driver, String sMSISDN, String AppURL) throws InterruptedException {

		Thread.sleep(5000);
		WebElement ClickTab = cellphoneNumber_radioButton(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);
		cellphoneNumber_radioButton(driver).click();
		cellphoneNumberInputFile(driver).sendKeys(sMSISDN);
		searchCustomerButton(driver).click();
		Thread.sleep(1000);

		String searchMessage = searchResponseMessage(driver).getText();

		String strPageURL = driver.getCurrentUrl();
		System.out.println("Page title: - " + strPageURL);

		results = "";
		results += "Actual: " + strPageURL + "\n";
		results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();
		if (searchMessage.equalsIgnoreCase("Success!")) {
			Assert.assertTrue(true);
		}

	}

	public void transactionReferenceNumber(WebDriver driver, String sTrans, String AppURL) throws InterruptedException {

		Thread.sleep(5000);
		WebElement ClickTab = transactionReferenceNumber_radioButton(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);
		transactionReferenceNumber_radioButton(driver).click();
		transactionReferenceInputField(driver).clear();
		transactionReferenceInputField(driver).sendKeys(sTrans);
		searchCustomerButton(driver).click();

		String searchMessage = searchResponseMessage(driver).getText();
		String strPageURL = driver.getCurrentUrl();
		System.out.println("Page title: - " + strPageURL);
		System.out.println("Search results - " + searchMessage);

		results = "";
		results += "Actual: " + strPageURL + "\n";
		results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		if (searchMessage.equalsIgnoreCase("Success!")) {
			Assert.assertTrue(true);
		}

	}

	public void ticketReferenceNumber(WebDriver driver, String AppURL, String ticketRef) throws InterruptedException {
		Thread.sleep(5000);
		WebElement ClickTab = ticketReferenceNumber_radioButton(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);
		ticketReferenceNumber_radioButton(driver).click();
		ticketReferenceNumberInoutFiled(driver).clear();
		ticketReferenceNumberInoutFiled(driver).sendKeys(ticketRef);
		searchCustomerButton(driver).click();

		setResultVariable(driver, AppURL);

	}

	public void id_passport_AsylumNumber(WebDriver driver, String id_passport_asylum, String AppURL)
			throws InterruptedException {

		Thread.sleep(5000);
		WebElement ClickTab = id_passport_asylumNumber(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);
		id_passport_asylumNumber(driver).click();
		idPassportAsylumNumber(driver).clear();
		idPassportAsylumNumber(driver).sendKeys(id_passport_asylum);
		searchCustomerButton(driver).click();

		setResultVariable(driver, AppURL);

	}

	public void FindHPID(WebDriver driver, String sMSISDN) throws InterruptedException {
		results = "";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		System.out.println("Confirming HPID page");
		Thread.sleep(6000);
		driver.findElement(By.xpath("//*[@id='tabone']/table/tbody/tr[10]/td[contains(text(), '" + sMSISDN + "')]"));
	}

	public String FindInvalidError(WebDriver driver) throws InterruptedException {
		results = "";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		Thread.sleep(2000);
		String Status = (driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[1]/div/p"))
				.getText());

		System.out.println("Invalid hp id search error message :" + Status);

		Assert.assertTrue(
				Status.contains("This Customer either does not exist on our system or is Blocked and set as Inactive"),
				"Error Message does not match");

		return Status;
	}

	private void setResultVariable(WebDriver driver, String AppURL) {
		String strPageURL = driver.getCurrentUrl();
		System.out.println("Page title: - " + strPageURL);
		results = "";
		results += "Actual: " + strPageURL + "\n";
		results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();
	}

	public void ConfirmPageAfterSearch(WebDriver driver, String sHPID) throws InterruptedException {

		Thread.sleep(30000);
		//driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		
		System.out.println("Confirming HPID page");

		results = "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

	
			driver.findElement(By.xpath("//*[@id='main']/div/div[1]/div[1]/h1/span[contains(text(), '" + sHPID + "')]"));


		if (driver.findElements(By.linkText("Documents")).size() > 0) {
			driver.findElement(By.linkText("Documents")).click();
		}
		if (driver.findElements(By.linkText("Acc Info")).size() > 0) {
			driver.findElement(By.linkText("Acc Info")).click();
		}
		if (driver.findElements(By.linkText("Access")).size() > 0) {
			driver.findElement(By.linkText("Access")).click();
		}

		driver.findElement(By.linkText("SMS")).click();
		driver.findElement(By.linkText("Information")).click();

		String Status = driver.findElement(By.xpath("//*[@id='main']/div/div[1]/div[1]/div/div/div[2]/div/h2"))
				.getText();

		if (Status.contains("Active")) {
			Assert.assertTrue(Status.contains("Active"), "Error Message does not match");
		}
		if (Status.contains("Rejected")) {
			System.out.println("Page confirmed with Rejected HP ID");
			Assert.assertTrue(Status.contains("Rejected"), "Error Message does not match");
		}
		SeleniumBaseClass.logger.log(LogStatus.PASS,"Page confirmed");

	}

}
