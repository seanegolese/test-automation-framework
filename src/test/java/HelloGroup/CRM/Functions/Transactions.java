package HelloGroup.CRM.Functions;
import java.sql.SQLException;
import java.sql.Statement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.POM.CRM_POM;
import Utility.FindElement;
import Utility.SeleniumBaseClass;

public class Transactions extends SeleniumBaseClass {

	public CRM_POM crmObjects = new CRM_POM();

	public void viewTransactions() {

		crmObjects.viewTransactionsButton(driver).click();
	}

	public void viewExpiredTransactions() {
		crmObjects.viewExpiredTransactionsButton(driver).click();
	}

	// Create transaction
	public void create(String sHPID, Statement statement) throws InterruptedException {
		System.out.println("Creating a Transaction of last recipient created");

		Reporter.log("STARTING TEST - Searching for last recipient created for   " + sHPID + " ");
		System.out.println("Searching for recipient in DB");

		try {

			String query = "SELECT HPB.*, HPBS.*  FROM HelloFin.HelloPaisaBeneficiary HPB left outer join HelloFin.HelloPaisaTransactions HPT on  HPT.BeneficiaryId = HPB.Id inner join HelloFin.HelloPaisaBanks HPBS on HPB.BankId =  HPBS.id where HPB.HPID = '"
					+ sHPID
					+ "' and HPB.status = '1' and HPT.id is null and HPB.CreationDate > date_sub(now(), interval 60 minute) ORDER BY HPB.id DESC LIMIT 1;";

			// statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				Thread.sleep(30000);
				WebElement RecipientScreen = driver.findElement(By.id("btnRecipientView"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen);

				driver.findElement(By.id("btnRecipientView")).click();
				String ID = rs.getString("ID");
				Reporter.log("Creating transaction for Beneficary ID " + ID + " ");
				String FirstName = rs.getString("FirstName");
				String LastName = rs.getString("Surname");
				String Bank = rs.getString("BankName");
				System.out.println("Bank is:" + Bank);
				System.out.println(ID);

				driver.findElement(By.id(ID)).click();
				Reporter.log("Found last recipient created for   " + sHPID + " with id " + ID + " for " + FirstName
						+ " and bank " + Bank + "  ");
				Thread.sleep(2000);
				// Clicking on
				driver.findElement(By.xpath("//*[@id='minAmount']")).click();
				Thread.sleep(2000);
				String TransactionAmmount = driver.findElement(By.xpath("//*[@id='transactionAmount']")).getText();
				String Totaltopay = driver.findElement(By.xpath("//*[@id='totalAmounttopay']")).getText();
				Reporter.log("On Clicking Minimun ammount the transaction ammount is: " + TransactionAmmount
						+ " and total to pay is: " + Totaltopay + "  ");
				Thread.sleep(2000);

				driver.findElement(By.xpath("//*[@id='maxAmount']")).click();
				Thread.sleep(2000);
				String TransactionAmmountMax = driver.findElement(By.xpath("//*[@id='transactionAmount']")).getText();
				String TotaltopayMax = driver.findElement(By.xpath("//*[@id='totalAmounttopay']")).getText();
				Reporter.log("On Clicking Maximum ammount the transaction ammount is: " + TransactionAmmountMax
						+ " and total to pay is: " + TotaltopayMax + "  ");

				driver.findElement(By.xpath("//*[@id='minAmount']")).click();

				driver.findElement(By.xpath("//*[@id='BOPCode']/option[2]")).click();
				driver.findElement(By.xpath("//*[@id='addTransaction_rpt']")).click();

				String NameSurname = driver.findElement(By.xpath("//*[@id='benefId']")).getText();
				System.out.println("The Screen Says: " + NameSurname);
				Assert.assertTrue(NameSurname.contains(FirstName), "Name not visible");
				Assert.assertTrue(NameSurname.contains(LastName), "Surname not visible");

				String BankName = driver.findElement(By.xpath("//*[@id='BankDetails']")).getText();
				System.out.println("Bank is: " + BankName);
				// Assert.assertTrue(BankName.contains(Bank), "Bank Not visible");

				Thread.sleep(5000);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}

		EnterOTPCreateTransaction(sHPID, statement);
	}

	// Enter OTP when creating a transaction
	private void EnterOTPCreateTransaction(String sHPID, Statement statement) throws InterruptedException {
		System.out.println("Entering OTP to confirm transaction");

		try {
			String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
					+ sHPID + "'";
			// statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				Thread.sleep(7000);
				String Otp = rs.getString("Otp");

				WebElement RecipientScreen = driver.findElement(By.xpath("//*[@id='confirmtransactionCode']"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen);

				driver.findElement(By.xpath("//*[@id='confirmtransactionCode']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='confirmtransactionCode']")).sendKeys(Otp);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='submitConfirm_rtp']")).click();
				System.out.println("Confirming Transaction created successfully");
				Thread.sleep(2000);

				Boolean isPresent = driver.findElements(By.xpath("//*[@id='alertTT']/div")).size() > 0;
				System.out.println(isPresent);

				if (isPresent == true) {

					String ActualMessage = driver.findElement(By.xpath("//*[@id='alertTT']/div")).getText();
					System.out.println(ActualMessage);
					Reporter.log("Entered OTP   " + Otp + " and recieved the message " + ActualMessage + " ");
					Assert.assertFalse(ActualMessage.contains("Transaction is greater than the remaining daily limit"),
							"Error daily limit does not match");
					Assert.assertFalse(ActualMessage.contains("You can only SEND CASH to 8 recipients for the month"),
							"Error 8 recipients does not match");
					// Assert.assertFalse(ActualMessage.contains("success"), "Error success does not
					// match");
					Assert.assertFalse(ActualMessage.contains("This Offer is invalid"),
							"Error Offer is invalid does not match");

					results = "";
					results += "Actual Message is " + ActualMessage + "" + "\n";

				} else {

					System.out.println("Error messages not found");

				}

				Thread.sleep(10000);
				driver.findElement(
						By.xpath("//*[@id='main']/div/div[1]/div[1]/h1/span[contains(text(), '" + sHPID + "')]"));

				Thread.sleep(10000);
				try {
					String query2 = "SELECT *  FROM HelloFin.HelloPaisaTransactions where HPID = '" + sHPID
							+ "' and MatchStatus = '20' and PartnerStatus = '60' and TransactionDate > date_sub(now(), interval 5 minute) ORDER BY id DESC LIMIT 1;";

					// statement = connection.createStatement();
					rs = statement.executeQuery(query2);

					if (!rs.isBeforeFirst()) {
						System.out.println("No Data Returned");
						Reporter.log("No data found on the " + query2 + " ");
						Assert.fail();
					}

					results = "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

					while (rs.next()) {
						System.out.println("Confirming Transaction created successfully in DB");
						String ReferenceNumber = rs.getString("RefNumber");
						String Amount = rs.getString("LocalAmount");

						Reporter.log("ENDING TEST - Created transaction referance   " + ReferenceNumber
								+ " for local ammount " + Amount + " ");

						Thread.sleep(5000);
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					Assert.fail();
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	// Cancel transaction
	public void cancel(WebDriver driver, String sHPID, String TransID, String ref,Statement statement) throws InterruptedException {
		
		try
		{
		System.out.println("Cancelling Transaction");

		Reporter.log("STARTING TEST - Searching for last recipient created for   " + sHPID + " ");

		FindElement.ElementXpath("//*[@id='Trans']");
		Thread.sleep(500);

		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='btnTransactionDeleted']")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//*[@id=\"tblCancelTransactions_filter\"]/label/input")).sendKeys(ref);

		Thread.sleep(2000);
		FindElement.ElementXpath("//*[@id='transHide_" + TransID + "']/td[11]/i");

		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}

	}

	// Enter OTP creating transaction
	private void enterOTPforCancelTransaction(String sHPID, String TransID, String Ref,Statement statement) throws InterruptedException {
		System.out.println("Entering OTP to confirm transaction");

		Thread.sleep(20000);

		try {
			String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
					+ sHPID + "'";
			//statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				Thread.sleep(7000);
				String Otp = rs.getString("Otp");
				driver.findElement(By.xpath("//*[@id='confirmVerificationCodeCancel']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='confirmVerificationCodeCancel']")).sendKeys(Otp);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='" + TransID + "']/div/div/div[2]/div[2]/button[1]")).click();

				Thread.sleep(10000);
				driver.findElement(
						By.xpath("//*[@id='main']/div/div[1]/div[1]/h1/span[contains(text(), '" + sHPID + "')]"));

				System.out.println("Confirming Cancel");

				Thread.sleep(30000);
				try {
					String query2 = "SELECT *  FROM HelloFin.HelloPaisaTransactions where Refnumber = '" + Ref
							+ "' and MatchStatus = '22'  ORDER BY id DESC LIMIT 1;";

					//statement = connection.createStatement();
					rs = statement.executeQuery(query2);

					if (!rs.isBeforeFirst()) {
						System.out.println("No Data Returned");
						Reporter.log("No data found on the " + query2 + " ");
						Assert.fail();
					}

					results = "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

					while (rs.next()) {
						System.out.println("Confirming Transaction created successfully in DB");
						String ReferenceNumber = rs.getString("RefNumber");
						String Amount = rs.getString("LocalAmount");

						Reporter.log("ENDING TEST - Canceled transaction referance   " + ReferenceNumber
								+ " for local ammount " + Amount + " ");

					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					Assert.fail();
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

}
