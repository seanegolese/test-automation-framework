package HelloGroup.CRM.Functions;

import java.sql.SQLException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Database;
import Utility.SeleniumBaseClass;

public class UserRoles extends SeleniumBaseClass {

	public Database database = new Database();

	public UserRoles() {
	}

	public int SetUserRole(String myHgId, String roleTestCaseClassName) throws InterruptedException {
		int affectedRows = 0;
		String level = "";

		switch (roleTestCaseClassName) {
		case "ConsultantRole":
			level = "rgl";
			break;
		case "LeadConsultantRole":
			level = "ldc";
			break;
		case "ManagerRole":
			level = "mng";
			break;
		case "StoreStaffRole":
			level = "stf";
			break;
		case "SystemAdminRole":
			level = "sys";
			break;
		case "TeamLeadRole":
			level = "lv3";
			break;
		case "VoucherConsultantRole":
			level = "lv4";
			break;
		case "TechSupportRole":
			level = "lv5";
			break;
		}

		String query = "UPDATE HelloFin.HelloAdmin SET Level='" + level + "' WHERE HgAdminId = '" + myHgId + "' ";

		try {
			affectedRows = database.setUserRolesInDatabase(query);
			System.out.println("Set role in database number of affected rows:" + affectedRows);
			logger.log(LogStatus.PASS, "Role set");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.log(LogStatus.FAIL, e.getMessage());
			System.out.println("Role not updated");
			e.printStackTrace();
			Assert.fail();
		}

		return affectedRows;
	}

	public void CheckReportsOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Reports Option");
			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Reports')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Reports')]")).click();

			logger.log(LogStatus.PASS, "Check Reports Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}

	}

	public void CheckCampaignEditorOption() throws InterruptedException {
		try {
			System.out.println("\nChecking CampaignEditor Option");
			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Campaign Editor')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Campaign Editor')]")).click();

			logger.log(LogStatus.PASS, "Check Campaign Editor Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

	public void CheckManageClassificationOption() throws InterruptedException {
		try {
			System.out.println("\nChecking ManageClassification Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Manage Classifications')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Manage Classifications')]")).click();

			logger.log(LogStatus.PASS, "Check Manage Classification Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}

	}

	public void CheckManageVouchersOption() throws InterruptedException {
		try {
			System.out.println("\nChecking ManageVouchers Option");
			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Manage Vouchers')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Manage Vouchers')]")).click();

			logger.log(LogStatus.PASS, "Check Manage Vouchers Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}

	}

	public void CheckBeneficiaryChangeCABSOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Beneficiary change CABS Option");
			WebElement ClickTab = driver.findElement(By.xpath("/html/body/aside[1]/div/section/ul/li[5]/a/span[2]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("/html/body/aside[1]/div/section/ul/li[5]/a/span[2]")).click();

			logger.log(LogStatus.PASS, "Check Beneficiary ChangeCABS Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}

	}

	public void CheckMatchTransactionOption() throws InterruptedException {

		try {
			System.out.println("\nChecking MatchTransaction Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Match Transactions')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Match Transactions')]")).click();

			logger.log(LogStatus.PASS, "Check Match Transaction Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

	public void GetCurrentUserAndRole() throws InterruptedException {
		try {
			System.out.println("\nChecking Current user and role.");

			WebElement ClickTab = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/ul[2]/li[1]/a/span/span[2]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			System.out.println(ClickTab.getText());

			logger.log(LogStatus.PASS, "Get Current User And Role");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}

	}

	public void CheckSystemUsersOption() throws InterruptedException {
		try {
			System.out.println("\nChecking System Users Option.");
			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'System Users')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'System Users')]")).click();

			logger.log(LogStatus.PASS, "Check System Users Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

	public void CheckMyTicketsOption() throws InterruptedException {
		try {
			System.out.println("\nChecking MyTickets Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'My Tickets')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);
			driver.findElement(By.xpath("//*[contains(text(),'My Tickets')]")).click();

			logger.log(LogStatus.PASS, "Check My Tickets Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
	}

	public void CheckDashboardOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Dashboard Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Dashboard')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Dashboard')]")).click();

			logger.log(LogStatus.PASS, "Check Dashboard Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
	}

	public void CheckCustomerSearchOption() throws InterruptedException {
		try {
			System.out.println("\nChecking CustomerSearch Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Customer Search')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Customer Search')]")).click();

			logger.log(LogStatus.PASS, "Check Customer Search Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
	}

	public void CheckSearchOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Search Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Search')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Search')]")).click();

			logger.log(LogStatus.PASS, "Check Search Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
	}

	public void CheckTicketsOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Tickets Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Tickets')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Tickets')]")).click();

			logger.log(LogStatus.PASS, "Check Tickets Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

	public void CheckCampaignOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Campaign Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Campaign')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);
			driver.findElement(By.xpath("//*[contains(text(),'Campaign')]")).click();
			logger.log(LogStatus.PASS, "Check Campaign Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

	public void CheckLockedAccountOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Locked Account Option");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Locked Account')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Locked Account')]")).click();

			logger.log(LogStatus.PASS, "Check Locked Account Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

	public void CheckFieldEmployeesOption() throws InterruptedException {
		try {
			System.out.println("\nChecking Field Employees Option.");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Field Employees')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			// driver.findElement(By.xpath("//*[contains(text(),'Field
			// Employees')]")).click();

			if (driver.findElements(By.xpath("//*[contains(text(),'Field Employees')]")).size() < 0) {
				logger.log(LogStatus.FAIL, "Field Employees Option not available");
				Assert.fail();
			}

			logger.log(LogStatus.PASS, "Check Field Employees Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

	public void CheckLogOutOption() throws InterruptedException {
		try {
			System.out.println("\nChecking LogOut Option\n");

			WebElement ClickTab = driver.findElement(By.xpath("//*[contains(text(),'Exit/Logout')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ClickTab);

			driver.findElement(By.xpath("//*[contains(text(),'Exit/Logout')]")).click();

			logger.log(LogStatus.PASS, "Check LogOut Option");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

}
