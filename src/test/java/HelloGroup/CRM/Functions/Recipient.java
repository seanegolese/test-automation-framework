package HelloGroup.CRM.Functions;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Utility.SeleniumBaseClass;

public class Recipient extends SeleniumBaseClass {

	
	
	
	public void viewRecipients(String sHPID, Statement statement) throws InterruptedException, SQLException {

		if (driver.findElement(By.id("RecipientsID")).isEnabled()) {
			if (driver.findElement(By.xpath("//*[@id=\"btnRecipientCreate\"]")).isEnabled()) {
				
				System.out.println("Confirm any available recipient");

				Thread.sleep(30000);
				WebElement RecipientScreen = driver.findElement(By.id("btnRecipientView"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen);

				Thread.sleep(10000);

				
				
				Thread.sleep(10000);
				String query2 ="SELECT HPB.*, HPBS.* FROM HelloFin.HelloPaisaBeneficiary HPB left outer join HelloFin.HelloPaisaTransactions HPT on  HPT.BeneficiaryId = HPB.Id inner join HelloFin.HelloPaisaBanks HPBS on HPB.BankId =  HPBS.id where HPB.HPID = '"+sHPID+"' and HPB.status = '1' and HPT.id is null and HPB.CreationDate > date_sub(now(), interval 60 minute) ORDER BY HPB.id DESC LIMIT 1";
				
				

				rs = statement.executeQuery(query2);

				while (rs.next()) {

					String BenefID = rs.getString("Id");
					String BenefName = rs.getString("FirstName");
					String BenefSurname = rs.getString("SurName");
					
					System.out.println("?"+BenefName+" "+BenefSurname);
				
				
				
				String recipientNames = driver.findElement(By.xpath("//*[@id=\"msgHide_"+BenefID+"\"]/td[2]")).getText();

				System.out.println("Recipient "+recipientNames+" exists");
							
				}
				
				
				
				

				
				

				
				results = "Method: " + new Object() {
				}.getClass().getEnclosingMethod().getName();

		
				

			} else {
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id=\"btnRecipientView\"]")).click();
			}

		} else {
			driver.findElement(By.id("RecipientsID")).click();
			driver.findElement(By.xpath("//*[@id=\"btnRecipientView\"]")).click();
		}

	}

	
	
	
	
	
	public void create(String Country, String RecieptMethod, String PayoutPartner, String RecieptFirstName,
			String RecieptLastName, String Number, String AccountNumber, String sHPID, Statement statement)
			throws InterruptedException {
		System.out.println("Creating Recipient");

		int num1, num2, num3; // 3 numbers in area code
		int set2, set3; // sequence 2 and 3 of the phone number

		Random generator = new Random();

		// Area code number; Will not print 8 or 9
		num1 = generator.nextInt(7) + 1; // add 1 so there is no 0 to begin
		num2 = generator.nextInt(8); // randomize to 8 because 0 counts as a
										// number in the generator
		num3 = generator.nextInt(8);
		set2 = generator.nextInt(643) + 100;
		set3 = generator.nextInt(8999) + 1000;

		String CellNumber = ("" + num1 + "" + num2 + "" + num3 + "" + set2 + "" + set3 + "");

		WebElement RecipientScreen = driver.findElement(By.xpath("//*[@id='RecipientsID']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen);

		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='RecipientsID']")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("btnRecipientCreate")).sendKeys(Keys.PAGE_DOWN);
		Thread.sleep(2000);
		driver.findElement(By.id("btnRecipientCreate")).click();
		Thread.sleep(2000);
		String ActualMessage = driver.findElement(By.xpath("//*[@id='CreateRecipient']/div")).getText();
		// System.out.println(ActualMessage);
		/// Reporter.log(ActualMessage);
		Assert.assertFalse(ActualMessage.contains("Customers are allowed a maximum of 8 recipients"),
				"More than 8 Recipients");

		/* Drop Down for Country */
		Thread.sleep(2000);
		Select selectByVisibleText = new Select(driver.findElement(By.id("drop_1")));
		selectByVisibleText.selectByVisibleText(Country);

		/* Drop Down for Collection Type */
		Thread.sleep(2000);
		Select selectByVisibleText1 = new Select(driver.findElement(By.id("drop_2")));
		selectByVisibleText1.selectByVisibleText(RecieptMethod);

		// System.out.println(PayoutPartner);
		/* Drop Down for PP */
		WebDriverWait wait = new WebDriverWait(driver, 20);

		// Wait until expected condition size of the dropdown increases and
		// becomes more than 1
		wait.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				Select select = new Select(driver.findElement(By.id("drop_4")));
				return select.getOptions().size() > 1;
			}
		});

		// To select the first option
		Select select = new Select(driver.findElement(By.id("drop_4")));
		select.selectByVisibleText(PayoutPartner);

		Thread.sleep(2000);
		driver.findElement(By.id("bName")).sendKeys(Keys.PAGE_DOWN);
		driver.findElement(By.id("bName")).clear();
		driver.findElement(By.id("bName")).sendKeys(RecieptFirstName);
		driver.findElement(By.id("bSurname")).clear();
		driver.findElement(By.id("bSurname")).sendKeys(RecieptLastName);
		driver.findElement(By.id("mobileNumbers")).clear();

		System.out.println("No: " + Number + "");
		// Sending Number
		if (Number.equalsIgnoreCase("n/a")) {

			driver.findElement(By.id("mobileNumbers")).sendKeys(CellNumber);

		} else if (Number != null) {

			driver.findElement(By.id("mobileNumbers")).sendKeys(Number);

		}

		Boolean isPresent = driver.findElements(By.xpath("//*[@id='BeneficiaryAccountNo']")).size() > 0;
		System.out.println("Found Account Number: " + isPresent);

		if (isPresent == true) {

			driver.findElement(By.xpath("//*[@id='BeneficiaryAccountNo']")).clear();
			driver.findElement(By.xpath("//*[@id='BeneficiaryAccountNo']")).sendKeys(AccountNumber);
			driver.findElement(By.id("add_rpt")).click();

		} else {

			driver.findElement(By.id("add_rpt")).click();

		}
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='alertC']/div[contains(text(), 'Verification Code has been sent')]"));
		Thread.sleep(2000);

		results = "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		EnterValidOTP(sHPID, statement);
	}

	
	
	private void EnterValidOTP(String sHPID, Statement statement) throws InterruptedException {
		System.out.println("Verifying New OTP");
		try {
			String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
					+ sHPID + "'";

			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned " + query + "");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			} else {
				System.out.println(query + " Returned Data");
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				String Otp = rs.getString("Otp");
				System.out.println(Otp);

				Thread.sleep(2000);

				driver.findElement(By.id("confirmOTP")).sendKeys(Otp);
				Thread.sleep(5000);
				WebElement ConfirmButton = driver.findElement(By.id("confirm_rtp"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ConfirmButton);

				driver.findElement(By.id("confirm_rtp")).click();
				Thread.sleep(10000);

				Boolean isPresent = driver.findElements(By.xpath("//*[@id='alertC']/div")).size() > 0;
				System.out.println(isPresent);

				if (isPresent == true) {

					String ActualMessage = driver.findElement(By.xpath("//*[@id='alertC']/div")).getText();
					Reporter.log(ActualMessage);
					Assert.assertFalse(ActualMessage.contains("Duplicate account number"),
							"Error Duplicate account number Message does not match");
					Assert.assertFalse(ActualMessage.contains("Unable to create recipient"), "Terrorist");
					Assert.assertFalse(ActualMessage.contains("NOTICE: Please wait"), "Not Creating Recipient");
					results = "";
					results += "Actual Message is " + ActualMessage + "" + "\n";

				} else {
					System.out.println("Created Successfully");
					Reporter.log("Created Successfully");

				}

				Thread.sleep(5000);

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}

		try {

			Thread.sleep(10000);
			String query2 = "SELECT * FROM HelloFin.HelloPaisaBeneficiary where HPid = '" + sHPID
					+ "' and CreationDate > date_sub(now(), interval 5 minute) and status = '1' ORDER BY id DESC LIMIT 1;";
			rs = statement.executeQuery(query2);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query2 + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				String BenefID = rs.getString("Id");
				Reporter.log("Created BenefID: " + BenefID + " ");

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	
	
	
	
	
	
	
	
	
	
	

	public void delete(String sHPID, Statement statement) throws InterruptedException {

		System.out.println("Deleting Recipient");
		Thread.sleep(5000);
		try {
			String query = "SELECT ID FROM HelloFin.HelloPaisaBeneficiary where Hpid = '" + sHPID+ "' and status = '1' and CreationDate > date_sub(now(), interval 120 minute) ORDER BY id DESC LIMIT 1";
			//statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				driver.findElement(
						By.xpath("//*[@id='btnRecipientView']")).click();
				String RecID = rs.getString("ID");
				System.out.println(RecID);
				Thread.sleep(2000);
				
				WebElement element = driver.findElement(By.xpath("//*[@id='msgHide_" + RecID + "']/td[11]/i"));
		   		JavascriptExecutor jse = (JavascriptExecutor)driver;
		   		jse.executeScript("arguments[0].scrollIntoView(true);", element);
		   		Thread.sleep(1000); 
		   		driver.findElement(By.xpath("//*[@id='msgHide_" + RecID + "']/td[11]/i")).click();

		   		
				Thread.sleep(2000);
				driver.switchTo().alert().accept();

				String Status = driver.findElement(By.xpath("//*[@id='myModalLabel']/h2")).getText();

				Assert.assertFalse(Status.contains("Delete"), "Page does not match");

				results = "";
				results += "Expected: " + "The Recipient ID to be deleted is: " + RecID + "" + "\n";
				results += "Actual: " + "The Recipient ID to be deleted is: " + RecID + "" + "\n";

				try {

					Thread.sleep(5000);
					String query2 = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
							+ sHPID + "';";
					//statement = connection.createStatement();
					rs = statement.executeQuery(query2);

					if (!rs.isBeforeFirst()) {
						System.out.println("No Data Returned");
						Assert.fail();
					}

					results = "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

					while (rs.next()) {
						String Otp = rs.getString("Otp");
						System.out.println(Otp);
						Thread.sleep(4000);
						driver.findElement(By.xpath("//*[@id='confirmVerificationCodeDelete']")).sendKeys(Otp);
						Thread.sleep(2000);
						driver.findElement(By.xpath("//*[@id='" + RecID + "']/div/div/div[2]/div[2]/button[1]"))
								.click();
						String ActualMessage = driver.findElement(By.xpath("//*/div/div/div[2]/div[1]/div/div/div"))
								.getText();
						Assert.assertFalse(ActualMessage.contains("Verification code do not match"),
								"Error Message does not match");

					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					Assert.fail();
				}

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}

	}

	
	
	
	
	
	
	
	
	
	
	
}
