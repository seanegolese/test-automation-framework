package HelloGroup.CRM.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CRM_POM {

	
	
	
	
	
	/*Framework constants */
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	
	
	public CRM_POM() {}
	
	
	
	/*Framework Objects */
	public WebElement username(WebDriver driver){return driver.findElement(By.name("username"));}
	public WebElement password(WebDriver driver) {return driver.findElement(By.name("password"));}
	public WebElement loginButton(WebDriver driver) {return driver.findElement(By.xpath("//button[@type='submit']"));}
	
	public WebElement profileDropDownButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='header']/div[2]/ul[2]/li[1]/a/span"));}
	public WebElement SignOutButton(WebDriver driver){return driver.findElement(By.xpath("//*[@id='header']/div[2]/ul[2]/li[1]/ul/li[3]/a"));}
	
	
	/*Customer Search */
	public WebElement hPID_radioButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[2]/div[1]/span[3]/label"));}
	public WebElement customerHdIdInputField(WebDriver driver) {return driver.findElement(By.id("hpId"));}
	
	public WebElement cellphoneNumber_radioButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[2]/div/span[1]/label"));}
	public WebElement cellphoneNumberInputFile(WebDriver driver) {return  driver.findElement(By.xpath("//*[@id='cellphone']"));}
	
	
    public WebElement transactionReferenceNumber_radioButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[2]/div/span[4]/label"));}
	public WebElement transactionReferenceInputField(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='transactionRef']"));}
    
    
	public WebElement ticketReferenceNumber_radioButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div/form/div[2]/div/span[5]/label"));}
	public WebElement ticketReferenceNumberInoutFiled(WebDriver driver) {return driver.findElement(By.xpath("//*[@id=\"ticketRef\"]"));}
    
	public WebElement id_passport_asylumNumber(WebDriver driver) {return driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div/form/div[2]/div/span[2]/label"));}
	public WebElement idPassportAsylumNumber(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='idNumber']"));}
    
	
	public WebElement searchResponseMessage(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[1]/div/h4[contains(text(), 'Success!')]"));}
	public WebElement searchCustomerButton(WebDriver driver) {return driver.findElement(By.xpath("//button[@type='submit']"));}	
	/*Customer search ends*/
	
	
	
	
	/*Add user objects */
	public WebElement firstNameInputField(WebDriver driver) {return driver.findElement(By.name("firstname"));}
	public WebElement surnameInputField(WebDriver driver) {return driver.findElement(By.name("surname"));}
	public WebElement cellPhoneInputField(WebDriver driver) {return driver.findElement(By.name("cellphone"));}
	public WebElement telephoneExtensionInputField(WebDriver driver) {return driver.findElement(By.name("extension"));}	
	public WebElement emailAddressInoutField(WebDriver driver) {return driver.findElement(By.name("email"));}		
	
	public WebElement accessGroupRightsDropDown(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[7]/div/div[1]/div[1]/input"));}	
	public WebElement selectOptionOnDropdown(WebDriver driver) { return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[7]/div/div[1]/div[1]/input"));}
	public WebElement saveUserButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[8]/button[2]"));}
	
	
	public WebElement waitMethod(WebDriver driver,String Xpath)
	{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));
		
		return element;
	}
	
	public WebElement transactionsButton(WebDriver driver) {return waitMethod(driver,"//*[@id=\"Trans\"]");}
	
	
	
	
	
	public WebElement viewTransactionsButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id=\"btnTransactionView\"]"));}
	
	public WebElement viewExpiredTransactionsButton(WebDriver driver) {return driver.findElement(By.xpath("//*[@id=\"btnTransactionExpired\"]"));}

	
	
}
