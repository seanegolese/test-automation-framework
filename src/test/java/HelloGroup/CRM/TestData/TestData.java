package HelloGroup.CRM.TestData;

import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestData {

	public static Object[][] excelDataProviderMethod(String folder, String excelName, String sheetName) {
		Object[][] retObjArr = { { folder } };
		try {
			File path = new File("");
			FileInputStream file = new FileInputStream(
					new File(path.getAbsolutePath() + "/Excel/CRM/" + folder + "/" + excelName + ".xlsx"));
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheet("" + sheetName + "");

			int rowCount = sheet.getPhysicalNumberOfRows() - 1;
			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			retObjArr = new Object[rowCount][colCount];
			for (int rownum = 0; rownum < rowCount; rownum++) {
				Row row = sheet.getRow(rownum + 1);

				for (int col = 0; col < colCount; col++) {
					retObjArr[rownum][col] = row.getCell(col).getStringCellValue();
				}
			}
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return retObjArr;
	}
}