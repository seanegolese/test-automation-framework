package HelloGroup.CRM.TestData;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Database {

	private static Statement statement;
	private static Connection connection;
	private static ResultSet rs;

	private static void open() {
		connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://10.0.0.80:3306/HelloFin", "mark.r", "Dravenm1903!");
			if (connection != null) {
				System.out.println("Connected to HelloFin Database...");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	// update database for test execution
	public int testDataPreparation(String query) throws SQLException {
		open();
		statement = connection.createStatement();
		return statement.executeUpdate(query);
	}

	// Set user role in the database before test execution
	public int setUserRolesInDatabase(String query) throws SQLException {
		open();
		statement = connection.createStatement();
		return statement.executeUpdate(query);
	}

	private static Object[][] mySqlDataProvider(String query) throws InterruptedException, IOException, SQLException {
		Object[][] retObjArr = null;
		try {

			open();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			while (rs.next()) {

				int rowCount = rs.getRow();

				retObjArr = new Object[rowCount][3];

				for (int row = 0; row < rowCount; row++) {
					System.out.println("Row number " + rs.getRow());
					retObjArr[row][0] = rs.getString("HPid");
					retObjArr[row][1] = rs.getString("transid");
					retObjArr[row][2] = rs.getString("Ref");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return retObjArr;
	}

	@DataProvider(name = "mysql-data-provider")
	public static Object[][] cancelTransactionDataProvider() throws InterruptedException, IOException, SQLException {
		String query = "SELECT HT.id as transid, HPC.HPid as HPid, HT.RefNumber as Ref, HT.LocalAmount as localammount, HT.ForeignAmount as foreignammount, HT.TransactionDate as txdate FROM HelloFin.HelloPaisaTransactions HT inner join HelloPaisaCustomer HPC on HT.HpId = HPC.HpId where HT.MatchStatus = 20 and PartnerStatus = 60 and HPC.Status = 1 order by HT.id desc limit 1";
		return mySqlDataProvider(query);
	}

	@Test(dataProvider = "paytransaction-test-data", dataProviderClass = Excel.class)
	public void testDataPreparation(String sHPID, String sUsername, String sPassword) throws SQLException {

		// Create recipient test data(pre-condition)
		System.out.println("Updating previous transactions to 22");
		testDataPreparation("Update HelloFin.HelloPaisaBeneficiary set status = 5 where HpId = '" + sHPID + "'");

		// Create transaction test data(pre-condition)
		testDataPreparation(
				"UPDATE HelloFin.HelloPaisaTransactions SET MatchStatus=22, transactiondate = '2016-08-22 00:00:00', PartnerStatus = 60 WHERE HPId = '"
						+ sHPID + "' and MatchStatus in ('20','21') ;");

	}

	// This method is executed when there is no recipient created and create
	// transaction test case need to executed.
	public int createRecipient(String sHPID) throws SQLException {
		open();
		return testDataPreparation(
				"UPDATE HelloFin.HelloPaisaBeneficiary SET Status='1',creationDate=now() where HPID = '" + sHPID
						+ "' order by Id limit 10");
	}
	
	
	
	

}
