package HelloGroup.CRM.TestData;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

public class Excel extends TestData{
	


	
	@DataProvider(name = "roles-test-data")
	public static Object[][] userRolesDataProvider(ITestContext context) {
	String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "UserRoles", "Credentials");
	}

	
	
	
	@DataProvider(name = "checkbalance-test-data")
	public static Object[][] checkBalanceDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Transactional", "Check Balance");
	}

	@DataProvider(name = "transaction-test-data")
	public static Object[][] payTransactionDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Transactional", "Transaction Info");
	}

	@DataProvider(name = "invalid-SignIn-test-data")
	public static Object[][] signDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Authentication", "Invalid User Logins");
	}	
	
	@DataProvider(name = "edit-user-test-data")
	public static Object[][] editUserDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Authentication", "EditUser");
	}	
	
	@DataProvider(name = "add-voucher-test-data")
	public static Object[][] addVoucherDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Vouchers", "AddVoucher");
	}	
	
	
	
	@DataProvider(name = "change-password-test-data")
	public static Object[][] changePasswordDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Authentication", "ChangePassword");
	}	
	
	
	@DataProvider(name = "create-recipient-test-data")
	public static Object[][] createRecipientDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Transactional", "Recipient Info");
	}
	
	@DataProvider(name = "create-ticket-cust-test-data")
	public static Object[][] createTicketCustDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Tickets", "Create Ticket Customer");
	}
	
	@DataProvider(name = "create-ticket-desh-test-data")
	public static Object[][] createTicketDeshDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Tickets", "Create Tickets Dash");
	}
	
	
	@DataProvider(name = "forgot-password-test-data")
	public static Object[][] forgotPasswordDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Authentication", "Forgot Password");
	}
	

	@DataProvider(name = "pin-reset-test-data")
	public static Object[][] pinResetDataProvider(ITestContext context) {
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Authentication", "PinReset");
	}
	
			
	/*Parameters:
	 *Id number
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="ID information")
	public static Object[][] idNumberIDDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "CustomerSearch", "ValidIDNumber");
		
	}
	
	
	
	/*Parameters:
	 *invalid HPID
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="invalid HPID information")
	public static Object[][] InvalidhpIDDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "CustomerSearch", "Invalid HPID");
		
	}
	
	
	
	/*Parameters:
	 * HPID
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="HPID information")
	public static Object[][] hpIDDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "CustomerSearch", "Valid HPID");
		
	}
	
	
	
	
	/*Parameters:
	 *Cell Number
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="cell number information")
	public static Object[][] cellNumberDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "CustomerSearch", "ValidMSISDN");
		
	}
	
	
	
	
	
	
	/*Parameters:
	 *Ticket in progrss
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="ticket in progress information")
	public static Object[][] ticketInProgressDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "CustomerSearch", "ValidTicketNumber");
		
	}
	
	
	/*Parameters:
	 *Transaction reference number
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="transaction reference information")
	public static Object[][] transactionReferenceNumberDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "CustomerSearch", "ValidTransaction");
		
	}

	/*Parameters:
	 *Transaction reference number
	 * Username
	 * passworord
	 * Application url
	 */
	@DataProvider(name="add user information")
	public static Object[][] addUserDataProvider(ITestContext context)
	{
		String ExcelLocation = context.getCurrentXmlTest().getParameter("ExcelLocation");
		return excelDataProviderMethod(ExcelLocation, "Authentication", "AddUser");
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	

}
