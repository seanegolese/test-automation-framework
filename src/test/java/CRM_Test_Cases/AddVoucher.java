package CRM_Test_Cases;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class AddVoucher extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public AddVoucher() {
		setId("366");
	}

	@Test(priority = 1, dataProvider = "add-voucher-test-data",dataProviderClass=Excel.class)
	public void RunTestAddVoucher(String sUsername, String sPassword, String newVoucher_name, String newVoucher_Pin,
			String newAmount, String DReminder, String daysw) throws InterruptedException, IOException {

		logger = extent.startTest("Add Voucher");
		try
		{
		login.SignIn(driver, sUsername, sPassword, appurl);
		AddingNewVoucher(newVoucher_name, newVoucher_Pin, newAmount, DReminder, daysw);
		logger.log(LogStatus.PASS, "Voucher added successfully");
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}


	public void AddingNewVoucher(String newVoucher_name, String newVoucher_Pin, String newAmount, String DReminder,
			String daysw) throws InterruptedException {

		try {
			int num1, num2, num3; // 3 numbers in area code
			int set2, set3; // sequence 2 and 3 of the phone number

			Random generator = new Random();

			// Area code number; Will not print 8 or 9
			num1 = generator.nextInt(7) + 1; // add 1 so there is no 0 to begin
			num2 = generator.nextInt(8); // randomize to 8 because 0 counts as a
											// number in the generator
			num3 = generator.nextInt(8);
			set2 = generator.nextInt(643) + 100;
			set3 = generator.nextInt(8999) + 1000;

			String RandomNumber = ("" + num1 + "" + num2 + "" + num3 + "" + set2 + "");
			WebElement element = driver.findElement(By.xpath("html/body/aside[1]/div/section/div/div[2]/img"));
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(500);
			driver.findElement(By.xpath("html/body/aside[1]/div/section/ul/li[16]/a/span[2]")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("html/body/aside[1]/div/section/ul/li[14]/a/span[2]")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id='main']/div/div[1]/div[2]/div/a[1]/button")).click();

			Thread.sleep(4000);
			System.out.println("Enter the Voucher Details");
			driver.findElement(By.name("newVoucher_name")).sendKeys(newVoucher_name);
			driver.findElement(By.name("newVoucher_Pin")).sendKeys(newVoucher_Pin + RandomNumber);

			System.out.println(newVoucher_Pin + RandomNumber);

			driver.findElement(By.name("newAmount")).sendKeys(newAmount);

			/* Drop Down for Reminder */
			Thread.sleep(2000);
			Select selectByVisibleText1 = new Select(driver.findElement(By.id("Reminder")));
			selectByVisibleText1.selectByVisibleText(DReminder);

			/* Drop Down for Days */
			Thread.sleep(2000);
			Select selectByVisibleText2 = new Select(driver.findElement(By.id("DaysW")));
			selectByVisibleText2.selectByVisibleText(daysw);

			/* DSave Voucher Button */
			driver.findElement(By.xpath("//*[@id='saveNewVoucher']")).click();

			Thread.sleep(5000);

			Boolean isPresent = driver.findElements(By.xpath("//*[@id='main']/div/div/div[3]/div/div")).size() > 0;
			System.out.println(isPresent);

			if (isPresent == true) {

				String Status = driver.findElement(By.xpath("//*[@id='main']/div/div/div[3]/div/div")).getText();
				System.out.println("Message returned is :" + Status + "");
				Assert.assertFalse(Status.contains("FAILED"), "Failed");

			} else {

				driver.findElement(By.xpath("//*[@id='voucherTable_filter']/label/input")).click();
				driver.findElement(By.xpath("//*[@id='voucherTable_filter']/label/input"))
						.sendKeys(newVoucher_Pin + RandomNumber);
				
				logger.log(LogStatus.PASS, "click add");
			}
			
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}
}