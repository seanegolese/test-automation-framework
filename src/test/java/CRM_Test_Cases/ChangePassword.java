package CRM_Test_Cases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Database;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class ChangePassword extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	String username = "automation@hellogroup.co.za";
	String pass = "Hello1234";

	public ChangePassword() {
		setId("404");
	}

	@Test(priority = 1, dataProvider = "change-password-test-data", dataProviderClass = Excel.class)
	public void RunTestChangePassword(String sUsername, String sPassword, String currentpass, String newpass,
			String verifypass) throws InterruptedException, IOException {
		logger = extent.startTest("Change Password");
		login.SignIn(driver, username, pass, appurl);
		CreateNewPassword(pass, newpass, verifypass);
	}

	public void CreateNewPassword(String currentpass, String newpass, String verifypass) throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(By.xpath("/html/body/header/div[2]/ul[2]/li[1]/a/span/span[3]")).click();
		driver.findElement(By.xpath("//*[@id='header']/div[2]/ul[2]/li[1]/ul/li[1]/a/span/i")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div[1]/ul[1]/li[3]/a/i")).click();

		System.out.println("Fill in the Blanks to Change Password");
		driver.findElement(By.name("currentpass")).sendKeys(currentpass);
		driver.findElement(By.name("newpass")).sendKeys(newpass);
		driver.findElement(By.name("verifypass")).sendKeys(verifypass);
		driver.findElement(By.xpath("//*[@id='password']/form/div[2]/button[2]")).click();
		Thread.sleep(4000);

		String ActualMessage = "";
		if (driver.findElements(By.xpath("//*[@id='main']/div/div[2]/div[1]/div/div/p")).size() > 0) {
			ActualMessage = driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div[1]/div/div/p")).getText();
		}
		System.out.println(ActualMessage);

		if (ActualMessage.contains("you provided does not match your")) {
			Reporter.log("Password Change is unsucessfull: " + ActualMessage + "");
			logger.log(LogStatus.FAIL, "Password Change is unsucessfull: " + ActualMessage + "");
			Assert.fail();
		}

		String strPageURL = driver.getCurrentUrl();
		System.out.println("Page title: - " + strPageURL);

		if (ActualMessage.contains("Your Current Password does not match the one we have in the System")) {
			Reporter.log("Password Change is unsucessfull: " + ActualMessage + "");
			logger.log(LogStatus.FAIL, "Password Change is unsucessfull: " + ActualMessage + "");
			Assert.fail();
		} 
		
		logger.log(LogStatus.PASS, "Password Change is sucessfull");
		

	}
	

}
