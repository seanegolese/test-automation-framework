package CRM_Test_Cases;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class ForgotPassword extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;
	
	public ForgotPassword(){
		setId("2097");
	}
	
	
	  @Test(priority=1, dataProvider = "forgot-password-test-data",dataProviderClass=Excel.class)
		public void RunTestForgotPassword(String eMail) throws InterruptedException,IOException, GeneralSecurityException{
		  
		  logger = extent.startTest("Forgot Password");
		  
		    SignIn(eMail);
		    ConfirmingPasswordReset(eMail);
	}
	
	
	public void SignIn(String eMail) throws InterruptedException{ 
		try
		{
		        System.out.println("Forgot your Password?");
		        driver.navigate().to(appurl);
				String strPageTitle = driver.getTitle();
				System.out.println("Page title: - "+strPageTitle);
				
				driver.findElement(By.xpath("//*[@id='forget-password']")).click();
				driver.findElement(By.name("email")).sendKeys(eMail);
				driver.findElement(By.xpath("/html/body/div[2]/form[2]/div[2]/button[2]")).click();
				

				
				Thread.sleep(1000);
				String ActualMessage = driver.findElement(By.xpath("html/body/div[2]/form[2]/div[1]/span")).getText();
	              
				System.out.println(ActualMessage);
				
	            Assert.assertTrue(ActualMessage.contains("has been E-mailed to your mailbox"), "Error Message does not match");
	            
	            logger.log(LogStatus.PASS, "Login with email executed successfully");
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
            
                
	}
	
	  public void ConfirmingPasswordReset(String eMail)throws InterruptedException, GeneralSecurityException{
	  	  System.out.println("Confirming Password Reset");

	  	  Thread.sleep(2000);
	  	  try {
	            String query = "SELECT * FROM HelloFin.HelloAdmin where email = '" + eMail + "'";
	            statement = connection.createStatement();
	            rs = statement.executeQuery(query);
	            
	            if (!rs.isBeforeFirst()) {
	          	    System.out.println("No Data Returned");
	          	    Assert.fail();
	          	  }
	            
	            results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();

	            while(rs.next()){
	            	
	            	String Password = rs.getString("Password");
	            	System.out.println(Password);
	          	 
	 
	          	  
	              results = "";
	              results += "New Encrypted Password is "+ Password +"" + "\n";
	            }
	            
	  	  	  logger.log(LogStatus.PASS, "Forgot password executed successfully");
	        } catch (SQLException ex) {
	        	logger.log(LogStatus.FAIL, ex.getMessage());
	           ex.printStackTrace();
	           Assert.fail();
	  	  }
	  	  
	  	  
	    }
	  
}
