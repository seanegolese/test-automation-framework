package CRM_Test_Cases;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class PayTransaction extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	public Search search = new Search();

	public PayTransaction() {
		setId("2247");
	}

	@Test(priority = 1, dataProvider = "transaction-test-data", dataProviderClass = Excel.class)
	public void RunTestPayTransactions(String sHPID, String sUsername, String sPassword)
			throws InterruptedException, IOException {
		logger = extent.startTest("Pay transaction");
		login.SignIn(driver, sUsername, sPassword, appurl);
		search.hpId(driver, sHPID, appurl);
		PayTrans(sHPID);
	}

	public void PayTrans(String sHPID) throws InterruptedException, IOException {
		System.out.println("Paying a transaction on last referance number with match status  = 20");

		results = "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		URL url;

		try {
			Thread.sleep(30000);
			String query = "SELECT RefNumber,LocalAmount FROM HelloFin.HelloPaisaTransactions WHERE HpID  =  '" + sHPID
					+ "' and matchstatus = '20' and transactiondate > date_sub(now(), interval 40000 minute) ORDER BY id DESC LIMIT 1";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				logger.log(LogStatus.FAIL, "No data returned");
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				String RefNumber = rs.getString("RefNumber");
				String LocalAmount = rs.getString("LocalAmount");
				System.out.println("Referance Number is: " + RefNumber);

				if (ExcelLocation.contains("Staging")) {
					String a = "http://10.0.30.21/HelloPaisa/payments/payTestTransaction.php?ref=" + RefNumber
							+ "&amount=" + LocalAmount + "";
					url = new URL(a);
					URLConnection conn = url.openConnection();
					System.out.println("URL: - " + url);

					BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				} else {

					String b = "http://10.0.0.80/HelloPaisa/payments/payTestTransaction.php?ref=" + RefNumber
							+ "&amount=" + LocalAmount + "";
					url = new URL(b);
					URLConnection conn2 = url.openConnection();
					System.out.println("URL: - " + url);

					BufferedReader br = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
				}

				Thread.sleep(20000);
				System.out.println("Searching For Transaction in CRM");

				WebElement TransScreen = driver.findElement(By.xpath("//*[@id='Trans']"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", TransScreen);

				driver.findElement(By.xpath("//*[@id='Trans']")).click();
				// driver.findElement(By.xpath("//*[@id='tblTransactions_filter']")).click();

				Thread.sleep(6000);
				/* This is not working */
				driver.findElement(By.xpath("//*[@id='tblTransactions_filter']/label/input")).sendKeys(RefNumber);
				driver.findElement(By.xpath("//*[@id='tblTransactions_filter']/label/input")).sendKeys(Keys.ENTER);
				driver.findElement(By.xpath("//*[@id='tblTransactions_filter']/label/input")).sendKeys(Keys.PAGE_DOWN);
				Thread.sleep(5000);
				driver.findElement(By.xpath("//*[@id='tblTransactions']/tbody/tr[1]/td[contains(text(), 'Paid')]"));

				driver.findElement(
						By.xpath("//*[@id='tblTransactions']/tbody/tr[1]/td[contains(text(), '" + LocalAmount + "')]"));

				results = "";
				results += "Actual: " + url + "\n";
				results += "Method: " + new Object() {
				}.getClass().getEnclosingMethod().getName();

			}

			try {
				Thread.sleep(15000);
				String query2 = "SELECT PartnerReference,RefNumber,LocalAmount FROM HelloFin.HelloPaisaTransactions WHERE HpID  =  '"
						+ sHPID
						+ "' and matchstatus = '21' and transactiondate > date_sub(now(), interval 30 minute) ORDER BY id DESC LIMIT 1";
				statement = connection.createStatement();
				rs = statement.executeQuery(query2);

				if (!rs.isBeforeFirst()) {
					logger.log(LogStatus.FAIL, "No data returned");
					System.out.println("No Data Returned");
					Reporter.log("No data found on the " + query2 + " ");
					Assert.fail();
				}

				results = "Method: " + new Object() {
				}.getClass().getEnclosingMethod().getName();

				while (rs.next()) {

					String RefNumber = rs.getString("RefNumber");
					String LocalAmount = rs.getString("LocalAmount");
					String PartnerRef = rs.getString("PartnerReference");
					Reporter.log("Referance Number is: " + RefNumber);
					Reporter.log("Local Ammount is: " + LocalAmount);
					Reporter.log("Partner Ref is: " + PartnerRef);

					results += "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

				}
				logger.log(LogStatus.PASS, "Transaction paid successfully");
			} catch (SQLException ex) {
				logger.log(LogStatus.FAIL, ex.getMessage());
				ex.printStackTrace();
				Assert.fail();
			}

		} catch (SQLException ex) {

			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}
	}

}