package CRM_Test_Cases;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class SearchInvalidHPID extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	
	public Search search = new Search();
	
	
	public SearchInvalidHPID(){
		setId("2246");
	}

  
  @Test(priority=1, dataProvider = "invalid HPID information",dataProviderClass=Excel.class)
  	public void RunTestSearchInvalidHPID(String sHPID,String sUsername,String sPassword, String AppURL) throws InterruptedException{
	  logger = extent.startTest("Search Invalid HP ID");
		login.SignIn(driver, sUsername, sPassword, AppURL);
		search.hpId(driver, sHPID, AppURL);
		logger.log(LogStatus.PASS,search.FindInvalidError(driver));
  }

}