package CRM_Test_Cases;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class DeleteRecipient extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;
	
	public Search search = new Search();
	
	public DeleteRecipient(){
		setId("4833");
	}
	
	
	@Test(priority = 1, dataProvider = "transaction-test-data",dataProviderClass=Excel.class)
	public void RunTestDeleteRecipient(String sHPID, String sUsername, String sPassword)
			throws InterruptedException {
	
		logger = extent.startTest("Delete Recipient");
		try
		{
		
		login.SignIn(driver, sUsername, sPassword, appurl);
		search.hpId(driver, sHPID, appurl);
		search.ConfirmPageAfterSearch(driver, sHPID);
		DeletingRecipient(sHPID);
		
		logger.log(LogStatus.PASS, "Recipient deleted successfully");
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}
	

	
	public void DeletingRecipient(String sHPID) throws InterruptedException {

		System.out.println("Deleting Recipient");
		Thread.sleep(5000);
		try {
			String query = "SELECT ID FROM HelloFin.HelloPaisaBeneficiary where Hpid = '" + sHPID
					+ "' and status = '1' and CreationDate > date_sub(now(), interval 120 minute) ORDER BY id DESC LIMIT 1";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				driver.findElement(
						By.xpath("//*[@id='btnRecipientView']")).click();
				String RecID = rs.getString("ID");
				System.out.println(RecID);
				Thread.sleep(2000);
				
				WebElement element = driver.findElement(By.xpath("//*[@id='msgHide_" + RecID + "']/td[11]/i"));
		   		JavascriptExecutor jse = (JavascriptExecutor)driver;
		   		jse.executeScript("arguments[0].scrollIntoView(true);", element);
		   		Thread.sleep(1000); 
		   		driver.findElement(By.xpath("//*[@id='msgHide_" + RecID + "']/td[11]/i")).click();

		   		
				Thread.sleep(2000);
				driver.switchTo().alert().accept();

				String Status = driver.findElement(By.xpath("//*[@id='myModalLabel']/h2")).getText();

				Assert.assertFalse(Status.contains("Delete"), "Page does not match");

				results = "";
				results += "Expected: " + "The Recipient ID to be deleted is: " + RecID + "" + "\n";
				results += "Actual: " + "The Recipient ID to be deleted is: " + RecID + "" + "\n";

				try {

					Thread.sleep(5000);
					String query2 = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
							+ sHPID + "';";
					statement = connection.createStatement();
					rs = statement.executeQuery(query2);

					if (!rs.isBeforeFirst()) {
						System.out.println("No Data Returned");
						Assert.fail();
					}

					results = "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

					while (rs.next()) {
						String Otp = rs.getString("Otp");
						System.out.println(Otp);
						Thread.sleep(4000);
						driver.findElement(By.xpath("//*[@id='confirmVerificationCodeDelete']")).sendKeys(Otp);
						Thread.sleep(2000);
						driver.findElement(By.xpath("//*[@id='" + RecID + "']/div/div/div[2]/div[2]/button[1]"))
								.click();
						String ActualMessage = driver.findElement(By.xpath("//*/div/div/div[2]/div[1]/div/div/div"))
								.getText();
						Assert.assertFalse(ActualMessage.contains("Verification code do not match"),
								"Error Message does not match");

					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					Assert.fail();
				}

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}

	}

}