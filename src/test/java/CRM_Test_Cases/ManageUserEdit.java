package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class ManageUserEdit extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;
	
	public ManageUserEdit(){
		setId("379");
	}
	
	
	     @Test(priority=1, dataProvider = "edit-user-test-data", dataProviderClass=Excel.class)
		public void RunTestEditUser(String sUsername, String sPassword, String UserEmail ) throws InterruptedException, IOException{
	    	 logger = extent.startTest("Edit user");
			login.SignIn(driver, sUsername, sPassword, appurl);
			EditUser(UserEmail);
			}

		public void EditUser(String UserEmail)throws InterruptedException  {
			try
			{
				
			
			WebElement element = driver.findElement(By.xpath("html/body/aside[1]/div/section/div/div[2]/img"));
	   		JavascriptExecutor jse = (JavascriptExecutor)driver;
	   		jse.executeScript("arguments[0].scrollIntoView(true);", element);
	   		Thread.sleep(1000); 
	   		driver.findElement(By.xpath("html/body/aside[1]/div/section/ul/li[16]/a/span[2]")).click();
			
			
			 int num1, num2, num3; //3 numbers in area code
		     int set2, set3; //sequence 2 and 3 of the phone number 
		     
			 Random generator = new Random();
		      
		      //Area code number; Will not print 8 or 9
		      num1 = generator.nextInt(7) + 1; //add 1 so there is no 0 to begin  
		      num2 = generator.nextInt(8); //randomize to 8 because 0 counts as a number in the generator
		      num3 = generator.nextInt(8);
		      set2 = generator.nextInt(643) + 100;
		      set3 = generator.nextInt(8999) + 1000;
		      
		     String RandomNumber = ( ""+ num1 + ""+ num2 + "" + num3 + "" + set2 + "" + set3 +"");
		     
		     Thread.sleep(4000);
			 WebElement menu = driver.findElement(By.xpath("//*[@id='users']/li[2]/a/span")); // the triger event element
			 Actions build = new Actions(driver); // heare you state ActionBuider
			 build.moveToElement(menu).build().perform(); // Here you perform hover mouse over the needed elemnt to triger the visibility of the hidden
			 WebElement m2m= driver.findElement(By.xpath("//*[@id='users']/li[2]/a/span"));//the previous non visible element
			 m2m.click();
			 
		     //Entering Search
		     Thread.sleep(4000);
			 driver.findElement(By.xpath("//*[@id='DataTables_Table_0_filter']/label/input")).click();
			 driver.findElement(By.xpath("//*[@id='DataTables_Table_0_filter']/label/input")).sendKeys(UserEmail);
			 driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[9]/a/img")).click();
			 
			
			 //Updating Cell Number
			 Thread.sleep(5000); 
			 System.out.println("Enter the User Details");
			 driver.findElement(By.name("cellphone")).clear();
			 driver.findElement(By.name("cellphone")).sendKeys(RandomNumber);
			 
			//Updating Password
			 Thread.sleep(5000); 
			 driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[8]/div/input")).sendKeys(RandomNumber);
			 
			 //Clicking Save/Add user button
			 Thread.sleep(5000); 
			 driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[9]/button[2]")).click();
			 
			 Thread.sleep(10000);
			 driver.findElement(By.xpath("//*[@id='DataTables_Table_0_filter']/label/input")).click();
			 
			 try {
			  		
		            String query = "SELECT count(*) as Count FROM HelloAdmin where CellNo = '"+ RandomNumber +"'and Status = '1'";
		            System.out.println(query);
		            statement = connection.createStatement();
		            rs = statement.executeQuery(query);
		            
		            
		            if (!rs.isBeforeFirst()) {
		          	    System.out.println("No Data Returned");
		          	    Assert.fail();
		          	  }
		            
		            results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();
		            
		            while(rs.next()){
		          	  
		            	int Count = rs.getInt("Count");
		            	System.out.println(Count);
		            	
		          	  if (Count >= 1 ) {
		            	    System.out.println("Cell Number Updated");
		            	    System.out.println(Count);
		            	  } 
		          	  
		          	  if (Count < 1 ) {
		        	    System.out.println("Cell Number Not updated");
		        	    System.out.println(Count);
		        	    Assert.fail();
		        	  } 
		          	  
		              results = "";
		              results += "Count of Users "+ Count +" created" + "\n";
		            }
		            
		            
		            logger.log(LogStatus.PASS, "Manage user edit executed successfully");
		        } catch (SQLException ex) {
		        	logger.log(LogStatus.FAIL, ex.getMessage());
		           ex.printStackTrace();
		           Assert.fail();
		        }
			 
			}
			catch(Exception ex)
			{
		       	logger.log(LogStatus.FAIL, ex.getMessage());
		           ex.printStackTrace();
		           Assert.fail();
			}
				
		  }
	
				
}
