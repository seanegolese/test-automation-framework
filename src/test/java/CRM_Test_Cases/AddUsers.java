package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Database;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class AddUsers extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	public Database dbcom = new Database();

	// used to set case ID in Selenium base class
	public AddUsers() {
		setId("378");
	}

	@Test(priority = 1, dataProvider = "add user information", dataProviderClass = Excel.class, description = "Add User")
	public void RunTestAddUser(String sUsername, String sPassword, String firstname, String surname, String Role)
			throws InterruptedException, IOException {

		logger = extent.startTest("Add User");
		try {
			System.out.println("Test Case: " + new Object() {
			}.getClass().getEnclosingMethod().getName());

			login.SignIn(driver, sUsername, sPassword, appurl);

			AddUser(firstname, surname, Role);
			logger.log(LogStatus.PASS, "User added successfully");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

	public void AddUser(String firstname, String surname, String Role) throws InterruptedException {

		WebElement element = driver.findElement(By.xpath("html/body/aside[1]/div/section/div/div[2]/img"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500);
		driver.findElement(By.xpath("html/body/aside[1]/div/section/ul/li[16]/a/span[2]")).click();

		int num1, num2, num3; // 3 numbers in area code
		int set2, set3; // sequence 2 and 3 of the phone number

		Random generator = new Random();

		// Area code number; Will not print 8 or 9
		num1 = generator.nextInt(7) + 1; // add 1 so there is no 0 to begin
		num2 = generator.nextInt(8); // randomize to 8 because 0 counts as a
										// number in the generator
		num3 = generator.nextInt(8);
		set2 = generator.nextInt(643) + 100;
		set3 = generator.nextInt(8999) + 1000;

		String RandomNumber = ("" + num1 + "" + num2 + "" + num3 + "" + set2 + "" + set3 + "");
		// Finding hidden Element if cannot scroll down
		Thread.sleep(4000);
		WebElement menu = driver.findElement(By.xpath("//*[@id='users']/li[2]/a/span")); // the
																							// triger
																							// event
																							// element
		Actions build = new Actions(driver); // heare you state ActionBuider
		build.moveToElement(menu).build().perform(); // Here you perform hover
														// mouse over the needed
														// elemnt to triger the
														// visibility of the
														// hidden
		WebElement m2m = driver.findElement(By.xpath("//*[@id='users']/li[2]/a/span"));// the
																						// previous
																						// non
																						// visible
																						// element
		m2m.click();

		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id='main']/div/div[1]/div[2]/div/a[1]/button")).click();

		// Filling out the form
		Thread.sleep(10000);
		System.out.println("Entering User Details :" + firstname + " : " + surname + " : " + RandomNumber + " : "
				+ RandomNumber + "@hellogroup.co.za");
		driver.findElement(By.name("firstname")).sendKeys(firstname);
		driver.findElement(By.name("surname")).sendKeys(surname);
		driver.findElement(By.name("cellphone")).sendKeys(RandomNumber);
		driver.findElement(By.name("extension")).sendKeys(RandomNumber);
		driver.findElement(By.name("email")).sendKeys(RandomNumber + "@hellogroup.co.za");

		// Selecting Item from the dropdown list
		System.out.println(Role);
		WebElement webElement = driver
				.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[7]/div/div[1]/div[1]/input"));// You
																												// can
																												// use
																												// xpath,
																												// ID
																												// or
																												// name
																												// whatever
																												// you
																												// like
		driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[7]/div/div[1]/div[1]/input"))
				.sendKeys(Role);
		webElement.sendKeys(Keys.ENTER);

		Thread.sleep(5000);
		// Clicking Save/Add user button
		driver.findElement(By.xpath("//*[@id='main']/div/div[2]/form/div[2]/div[8]/button[2]")).click();

		Thread.sleep(2000);

		try {

			String query = "SELECT count(*) as Count FROM HelloAdmin where Email = '" + RandomNumber
					+ "@hellogroup.co.za'and Status = '1'";
			System.out.println(query);
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				int Count = rs.getInt("Count");
				System.out.println(Count);

				if (Count >= 1) {
					System.out.println("Email Address Created in database ");
					System.out.println(Count);
				}

				if (Count < 1) {
					System.out.println("User Not added ");
					System.out.println(Count);
					Assert.fail();
				}

				results = "";
				results += "Count of Users " + Count + " created" + "\n";
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}

	}

}
