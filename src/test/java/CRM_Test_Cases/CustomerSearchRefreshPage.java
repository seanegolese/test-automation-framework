package CRM_Test_Cases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CustomerSearchRefreshPage extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	
	public CustomerSearchRefreshPage(){
		setId("282");
	}
  
  @Test(priority=1, dataProvider = "ID information",dataProviderClass=Excel.class)
  	public void RunTestSearchValidHPID(String sIDNo,String sUsername,String sPassword, String AppURL) throws InterruptedException{
	  	logger = extent.startTest("Customer Search fresh");
		login.SignIn(driver, sUsername, sPassword, AppURL);
		Refresh(sIDNo,AppURL);
  }
  
  
  
	public void Refresh(String sIDNo, String AppURL)throws InterruptedException {
	  
	try
	{
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("html/body/aside[1]/div/section/ul/li[3]/a/span[2]")).click();
	  Thread.sleep(2000);
      driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[1]/h3[contains(text(), 'Search Customer By')]"));
      driver.findElement(By.xpath("//*[@id='cellphone']")).click();
      driver.findElement(By.xpath("//*[@id='main']/div/div[1]/div[2]/div/a/button")).click();
      Thread.sleep(10000);
      driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[1]/h3[contains(text(), 'Search Customer By')]"));
      driver.findElement(By.xpath("//*[@id='cellphone']")).click();
      
      String strPageURL = driver.getCurrentUrl();
      System.out.println("Page title: - "+strPageURL);
      
      results = "";
      results += "Actual: " + strPageURL + "\n";
      results += "Expected: " + ""+ AppURL +"pages/main.php" + "\n";
      results += "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();
      
      Assert.assertTrue(strPageURL.contains(""+ AppURL +"pages/main.php?crm=customerProfileSearch"), "Page title doesn't match");
      logger.log(LogStatus.PASS, "Page refreshed successfully");
		
		}
	catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}
}