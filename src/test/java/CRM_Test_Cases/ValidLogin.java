package CRM_Test_Cases;

import org.testng.annotations.Test;

import HelloGroup.CRM.TestData.Database;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class ValidLogin extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public Database db = new Database();

	public ValidLogin() {
		setId("2095");
	}	

	@Test(priority = 1, dataProvider = "roles-test-data", dataProviderClass = Excel.class)
	public void SignIn(String sUsername, String sPassword, String AppURL) throws InterruptedException {	
		logger=extent.startTest("Valid login");
		login.SignIn(driver, sUsername, sPassword, appurl);
	}

}
	