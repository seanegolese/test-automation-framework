package CRM_Test_Cases;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class InvalidLogin extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	
	public InvalidLogin(){
		setId("2096");
	}
	
	
	@Test(priority=1, dataProvider="invalid-SignIn-test-data",dataProviderClass=Excel.class)
	public void SignIn(String sUsername, String sPassword, String AppURL) { 
		
		logger = extent.startTest("Invalid SignIn");
		
			        System.out.println("Signing In to the app");
			        driver.navigate().to(AppURL);
					String strPageTitle = driver.getTitle();
					System.out.println("Page title: - "+strPageTitle);
	                driver.findElement(By.name("username")).sendKeys(sUsername);
	                driver.findElement(By.name("password")).sendKeys(sPassword);
	                driver.findElement(By.xpath("//button[@type='submit']")).click();
	                String strPageURL = driver.getCurrentUrl();
	                System.out.println("Page title: - "+strPageURL);
	                
	                results = "";
	                results += "Actual: " + strPageURL + "\n";
	                results += "Expected: " + ""+ AppURL +"jqueryComponents/userLogin.php" + "\n";
	                results += "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();
	                
	                Assert.assertTrue(strPageURL.equalsIgnoreCase(""+ AppURL +"jqueryComponents/userLogin.php"), "Page title doesn't match");
	                logger.log(LogStatus.PASS, "Invalid login executed successfully");
		}
	

	
	
}
		
