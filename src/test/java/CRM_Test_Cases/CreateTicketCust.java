package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.Functions.Tickets;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CreateTicketCust extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;
	
	
	public Search search = new Search();
	public Tickets tickets = new Tickets();
	
	public CreateTicketCust(){
		setId("301");
	}


  @Test(priority=1, dataProvider = "create-ticket-cust-test-data",dataProviderClass=Excel.class)
	public void RunTestTicketDash(String sHPID,String sUsername,String sPassword, String Class,String ClassType, String SubClass) throws InterruptedException, IOException{
	   
	  logger=extent.startTest("Create Customer Ticket");
	  	login.SignIn(driver, sUsername, sPassword, appurl);
		search.hpId(driver, sHPID, appurl);
		search.ConfirmPageAfterSearch(driver, sHPID);
		CreateTickets(Class,SubClass);
		ConfirmingTicketCreation(sHPID,ClassType);
}
  
  	public void CreateTickets(String Classification,String SubClassification) throws InterruptedException{
        System.out.println("Creating a ticket");
        Thread.sleep(3000);
        

        driver.findElement(By.xpath("//*[@id='btnCreate']")).click();
        Thread.sleep(3000);
        
        /*Drop Down for Classification*/
        Thread.sleep(2000);
        Select selectByVisibleText = new Select(driver.findElement(By.xpath("//*[@id='classification']")));
        selectByVisibleText.selectByVisibleText(Classification); 
        
        /*Drop Down for Sub Classification*/
        Thread.sleep(2000);
        Select selectByVisibleText1 = new Select (driver.findElement(By.xpath("//*[@id='subClassification']")));
        selectByVisibleText1.selectByVisibleText(SubClassification);
        
        
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='SaveInprogress_rtp']")).click();
        
        Thread.sleep(5000);
        
        results = "";
        results += "Actual: " + "New ticket for : "+ Classification +" created" + "\n";
        results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();
        logger.log(LogStatus.PASS, "New ticket for : "+ Classification +" created");
    
  }
  	
  	
  	
  	public void ConfirmingTicketCreation(String sHPID,String ClassType)throws InterruptedException{
    	  System.out.println("Checking DB for Ticket Creation");
    	  
    	  //Class Type needs to be specified on Excel Document
    	  
    	  Thread.sleep(10000);
    	  try {
              String query = "SELECT count(*) as Count, CL.ActivityRef FROM HelloFin.HelloCustomerLogging  CL inner join HelloFin.HelloAdmin HA on CL.CreatedBy = HA.HgAdminId  where CL.HpId = '" + sHPID + "' and CreatedDate > date_sub(now(), interval 0.5 minute) and Status = '1' and CL.Classification = '" + ClassType + "'";
              statement = connection.createStatement();
              rs = statement.executeQuery(query);
              
              if (!rs.isBeforeFirst()) {
            	    System.out.println("No Data Returned");
            	    Assert.fail();
            	    logger.log(LogStatus.FAIL, "No data returned when validating create ticket in database");
            	  }
              
              results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();

              while(rs.next()){
            	  
              	int Count = rs.getInt("Count");
              	String Ref = rs.getString("ActivityRef");
               
            	  if (Count > 1 ) {
              	    System.out.println("DUPLICATE TICKETS");
              	    System.out.println(Count);
              	    Reporter.log("Duplicate Tickets");
              	    Assert.fail();
              	  } 
            	  
            	  if (Count < 1 ) {
          	        System.out.println("NO DATA INSERTED");
          	        System.out.println(Count);
          	        Reporter.log("No Data inserted");
          	      logger.log(LogStatus.FAIL, "No data returned when validating create ticket in database");
          	        Assert.fail();
          	  } 
            	  
            	driver.findElement(By.linkText("Information")).sendKeys(Keys.PAGE_DOWN);
            	
            	Thread.sleep(5000);
            	
            	driver.findElement(By.xpath("//*[@id='btnInprogress']")).click(); 
            	
            	
            	Thread.sleep(2000);
            	
            	driver.findElement(By.xpath("//*[@id='DataTables_Table_1_filter']/label/input")).sendKeys(Ref);
            	
            	Thread.sleep(2000);
            	
            	
            	String Ticket = driver.findElement(By.xpath("//*[@id='DataTables_Table_1']/tbody/tr/td[2]")).getText();
                
                Assert.assertTrue(Ticket.contains(Ref), "Can not find ticket");
                
                Reporter.log("Count of tickets "+ Count +" created with Ref "+ Ref +" ");
            	  
                results = "";
                results += "Count of tickets "+ Count +" created "+ Ref +" "+ "\n";
              }
              
              logger.log(LogStatus.PASS, "Create ticket validated.");
          } catch (SQLException ex) {
        	  logger.log(LogStatus.FAIL, ex.getMessage());
             ex.printStackTrace();
             Assert.fail();
          }
      }

}