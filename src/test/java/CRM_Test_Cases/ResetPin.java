package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Login;
import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.FindElement;
import Utility.SeleniumBaseClass;

public class ResetPin extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	public Login login = new Login();
	public FindElement findElement = new FindElement();
	public Search search = new Search();

	// used to set case ID in Selenium base class
	public ResetPin() {
		setId("49974");
	}

	@Test(priority = 1, dataProvider = "pin-reset-test-data", dataProviderClass = Excel.class)
	public void RunTestTicketDash(String sUsername, String sPassword, String CellNumb, String myHgId)
			throws InterruptedException, IOException, SQLException {
		logger = extent.startTest("Reset Pin");

		ValuesBeforePinReset(myHgId);
		BlockNumberInCustomerTable(myHgId);
		login.SignIn(driver, sUsername, sPassword, appurl);
		PinReset(CellNumb);
		GetVerificationCode(myHgId);
		ValuesAfterPINReset(myHgId);
	}

	public void ValuesBeforePinReset(String myHgId) throws InterruptedException {

		Thread.sleep(5000);
		try {
			String query = "SELECT hp.Password, hc.StatusId FROM HelloFin.HelloCustomerOTP ho inner join HelloFin.HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloFin.HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hc.HgId = '"
					+ myHgId + "' ";

			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				logger.log(LogStatus.FAIL, "No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				String myCurrentStatusId = rs.getString("StatusId");
				System.out.println("Status ID before PIN Reset  : " + myCurrentStatusId);

				String myOldPin = rs.getString("Password");
				System.out.println("PIN before PIN Reset : " + myOldPin);

			}

			logger.log(LogStatus.PASS, "Value before pin reset");

		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}

	}

	public void BlockNumberInCustomerTable(String myHgId) throws InterruptedException {

		Thread.sleep(5000);
		try {

			String query = "UPDATE HelloFin.HelloCustomer SET StatusId=9 WHERE HgId = '" + myHgId + "' ";
			statement = connection.createStatement();
			int customerStatus = statement.executeUpdate(query);

			String query2 = "UPDATE HelloFin.HelloPaisaCustomer SET Status=9 WHERE HgId = '" + myHgId + "' ";
			statement = connection.createStatement();
			int PaisaCustomerStatus = statement.executeUpdate(query2);
			if (customerStatus == 1 && PaisaCustomerStatus == 1) {
				System.out.println("Cell phone Number blocked");
			}

			logger.log(LogStatus.PASS, " Block number in customer table");
		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}
	}

	public void PinReset(String myCellNo) throws InterruptedException {

		driver.findElement(By.id("cellphone")).sendKeys(myCellNo);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div/form/div[8]/button")).click();

		driver.findElement(By
				.xpath("//*[@id='main']/div/div[1]/div[1]/div/div/div[2]/div/h2[contains(text(), 'Account Locked')]"));
		Thread.sleep(10000);

		driver.findElement(By.linkText("Access")).click();
		Thread.sleep(500);

		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		driver.findElement(By.xpath("//*[@id=\"genVerCode\"]/span")).click();

		logger.log(LogStatus.PASS, "reseting pin");

	}

	public void GetVerificationCode(String myHgId) throws InterruptedException, SQLException {

		Thread.sleep(30000);

		String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloFin.HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloFin.HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hc.HgId = '"
				+ myHgId + "' ";
		statement = connection.createStatement();
		rs = statement.executeQuery(query);

		if (!rs.isBeforeFirst()) {
			logger.log(LogStatus.FAIL, "no varification code returned");
			System.out.println("No Data Returned");
			Reporter.log("No data found on the " + query + " ");
			Assert.fail();
		}

		results = "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		while (rs.next()) {
			String myOtp = rs.getString("Otp");
			System.out.println("OTP for the PIN Reset : " + myOtp);

			Thread.sleep(10000);
			driver.findElement(By.id("verificationCode")).sendKeys(myOtp);
			driver.findElement(By.xpath("//*[@id=\"submitResetPin\"]/span")).click();

			// Added to check for failure on pin reset
			String Message = driver.findElement(By.xpath("//*[@id='tabfour']/div[2]/div[2]/div/form/div[1]/div/p"))
					.getText();

			if (Message.contains("PIN reset failed, please try again later")) {
				logger.log(LogStatus.FAIL, "PIN reset failed, please try again later");
				System.out.println("PinResetFailed:" + Message);
				Assert.fail();
			}

			logger.log(LogStatus.PASS, "verification code successful");
		}

	}

	public void ValuesAfterPINReset(String myHgId) throws InterruptedException {

		driver.findElement(
				By.xpath("//*[@id='main']/div/div[1]/div[1]/div/div/div[2]/div/h2[contains(text(), 'Active')]"));

		try {

			String query = "SELECT hp.Password, hc.StatusId FROM HelloFin.HelloCustomerOTP ho inner join HelloFin.HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloFin.HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hc.HgId = '"
					+ myHgId + "' ";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				logger.log(LogStatus.FAIL, "No data found on the " + query + " ");
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				String myCurrentStatusId = rs.getString("StatusId");
				System.out.println("Status ID after PIN Reset : " + myCurrentStatusId);

				String myOldPin = rs.getString("Password");
				System.out.println("PIN after PIN Reset : " + myOldPin);

				if (myCurrentStatusId.matches("1")) {
					System.out.println("Status is now:" + myCurrentStatusId);
				} else {
					Assert.fail();
				}
				logger.log(LogStatus.PASS, "Values after pin reset ");
			}

		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}
	}

}
