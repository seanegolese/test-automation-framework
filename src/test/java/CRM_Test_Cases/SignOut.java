package CRM_Test_Cases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class SignOut extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public SignOut() {
		setId("2098");
	}

	@Test(dataProvider = "roles-test-data", dataProviderClass = Excel.class)
	public void SignInOut(String sUsername, String sPassword, String AppURL) throws InterruptedException {
		logger = extent.startTest("SignOut");
		try {

			login.SignIn(driver, sUsername, sPassword, appurl);
			String strPageURL = driver.getCurrentUrl();
			System.out.println(" Login Successfully, now it is the time to Log Off Customer Care CRM.");

			// Find the element that's ID attribute is 'account_logout' (Log Out)
			driver.findElement(By.xpath("//*[@id='header']/div[2]/ul[2]/li[1]/a/span")).click();
			driver.findElement(By.xpath("//*[@id='header']/div[2]/ul[2]/li[1]/ul/li[3]/a")).click();

			Thread.sleep(4000);
			String strPageURL2 = driver.getCurrentUrl();
			System.out.println(strPageURL2);
			// Added assertion to check the login url, if fails//
			Assert.assertTrue(strPageURL2.equalsIgnoreCase("" + appurl + "index.html"), "Page title doesn't match");

			System.out.println(" Logged Out of Customer Care CRM Successfully");

			results = "";
			results += "Actual: " + strPageURL + "\n";
			results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();
			logger.log(LogStatus.PASS, "SignOut successful");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

}
