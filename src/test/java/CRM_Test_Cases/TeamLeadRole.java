package CRM_Test_Cases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.UserRoles;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class TeamLeadRole extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public UserRoles role = new UserRoles();

	public TeamLeadRole() {
		setId("2095");
	}

	@Test(priority = 1, dataProvider = "roles-test-data", dataProviderClass = Excel.class)
	public void RunTestTicketDash(String sUsername, String sPassword, String myHgId)
			throws InterruptedException, IOException {
		
		logger = extent.startTest("Team Lead Roles");
		
		role.SetUserRole(myHgId, this.getClass().getSimpleName().toString());
		login.SignIn(driver, sUsername, sPassword, appurl);
		role.GetCurrentUserAndRole();
		role.CheckMyTicketsOption();
		role.CheckDashboardOption();
		role.CheckCustomerSearchOption();
		role.CheckSearchOption();
		role.CheckBeneficiaryChangeCABSOption();
		role.CheckMatchTransactionOption();
		role.CheckTicketsOption();
		role.CheckCampaignOption();
		role.CheckLockedAccountOption();
		role.CheckFieldEmployeesOption();
		role.CheckLogOutOption();
		logger.log(LogStatus.PASS, "Team Lead roles confirmed");
	}

}
