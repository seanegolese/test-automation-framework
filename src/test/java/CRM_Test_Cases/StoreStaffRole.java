package CRM_Test_Cases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.Functions.UserRoles;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class StoreStaffRole extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public UserRoles role = new UserRoles();
	public Search search = new Search();
	
	
	public StoreStaffRole() {
		setId("2095");
	}

	@Test(priority = 1, dataProvider = "roles-test-data", dataProviderClass = Excel.class)
	public void RunTestTicketDash(String sUsername, String sPassword, String myHgId)
			throws InterruptedException, IOException {
		logger = extent.startTest("Store staff roles");
		try {
			role.SetUserRole(myHgId, this.getClass().getSimpleName().toString());
			login.SignIn(driver, sUsername, sPassword, appurl);
			role.GetCurrentUserAndRole();
			role.CheckMyTicketsOption();
			role.CheckDashboardOption();
			role.CheckCustomerSearchOption();
			role.CheckSearchOption();
			role.CheckLogOutOption();
			logger.log(LogStatus.PASS, "Store staff roles confirmed");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}
	

}
