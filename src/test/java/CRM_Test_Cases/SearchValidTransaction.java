package CRM_Test_Cases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class SearchValidTransaction extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public Search search = new Search();

	public SearchValidTransaction() {
		setId("272");
	}

	@Test(priority = 1, dataProvider = "transaction reference information",dataProviderClass=Excel.class)
	public void RunTestSearchValidHPID(String sTrans, String sUsername, String sPassword, String AppURL)
			throws InterruptedException {
		logger = extent.startTest("Search valid transaction");
		try {
			login.SignIn(driver, sUsername, sPassword, AppURL);
			search.transactionReferenceNumber(driver, sTrans, AppURL);
			FindTrans(sTrans);
			logger.log(LogStatus.PASS, "Search transaction executed successfully");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}


	public void TransSearch(String sTrans, String AppURL) throws InterruptedException {

		try {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[2]/div/span[4]/label"))
					.click();
			driver.findElement(By.xpath("//*[@id='transactionRef']")).clear();
			;
			driver.findElement(By.xpath("//*[@id='transactionRef']")).sendKeys(sTrans);
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath(
					"//*[@id='main']/div/div[2]/div/div[2]/div/form/div[1]/div/h4[contains(text(), 'Success!')]"));
			String strPageURL = driver.getCurrentUrl();
			System.out.println("Page title: - " + strPageURL);

			results = "";
			results += "Actual: " + strPageURL + "\n";
			results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			Assert.assertTrue(strPageURL.equalsIgnoreCase("" + AppURL + "pages/main.php"), "Page title doesn't match");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
	}

	public void FindTrans(String sTrans) throws InterruptedException {

		try {
			results = "";
			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();
			Thread.sleep(10000);

			WebElement TransScreen = driver.findElement(By.xpath("//*[@id='Trans']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", TransScreen);
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id='Trans']")).click();
			logger.log(LogStatus.PASS, "find transaction");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}

	}

}