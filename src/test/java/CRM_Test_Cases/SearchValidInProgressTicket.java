package CRM_Test_Cases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class SearchValidInProgressTicket extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public SearchValidInProgressTicket() {
		setId("273");
	}

	@Test(priority = 1, dataProvider = "ticket in progress information",dataProviderClass=Excel.class)
	public void RunTestSearchValidHPID(String sTicket, String sUsername, String sPassword, String AppURL)
			throws InterruptedException {
		
		logger = extent.startTest("Search valid in progress ticket");
		try
		{
		login.SignIn(driver, sUsername, sPassword, AppURL);
		TicketSearch(sTicket, AppURL);
		FindTicket(sTicket);
		
		logger.log(LogStatus.PASS, "Search valid ticket in progress executed successsfully");
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

	public void TicketSearch(String sTicket, String AppURL) throws InterruptedException {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[2]/div/span[5]/label"))
					.click();
			driver.findElement(By.xpath("//*[@id='ticketRef']")).sendKeys(sTicket);
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath(
					"//*[@id='main']/div/div[2]/div/div[2]/div/form/div[1]/div/h4[contains(text(), 'Success!')]"));
			String strPageURL = driver.getCurrentUrl();
			System.out.println("Page title: - " + strPageURL);

			results = "";
			results += "Actual: " + strPageURL + "\n";
			results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			Assert.assertTrue(strPageURL.equalsIgnoreCase("" + AppURL + "pages/main.php"), "Page title doesn't match");
			logger.log(LogStatus.PASS, "Ticket search : " + AppURL + "pages/main.php");

	}

	public void FindTicket(String sTicket) throws InterruptedException {
			results = "";
			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			WebElement InProgress = driver.findElement(By.xpath("//*[@id='btnInprogress']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InProgress);

			Thread.sleep(500);
			Thread.sleep(10000);
			driver.findElement(By.xpath("//*[@id='DataTables_Table_1_filter']/label/input")).click();
			/* This is not working */
			driver.findElement(By.xpath("//*[@id='DataTables_Table_1_filter']/label/input")).sendKeys(sTicket);
			driver.findElement(By.xpath("//*[@id='DataTables_Table_1_filter']/label/input")).sendKeys(Keys.ENTER);
			driver.findElement(By.xpath("//*[@id='DataTables_Table_1_filter']/label/input")).sendKeys(Keys.PAGE_DOWN);

			driver.findElement(
					By.xpath("//*[@id='DataTables_Table_1']/tbody/tr/td[2][contains(text(), '" + sTicket + "')]"));
			logger.log(LogStatus.PASS, "find ticket");
	}

}