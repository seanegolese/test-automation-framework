package CRM_Test_Cases;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;
import junit.framework.Assert;

public class SearchValidIDNumber extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public Search search = new Search();
	
	
	public SearchValidIDNumber(){
		setId("270");
	}
  
  
  @Test(priority=1, dataProvider = "ID information",dataProviderClass=Excel.class)
  	public void RunTestSearchValidHPID(String sIDNo,String sUsername,String sPassword, String AppURL) throws InterruptedException{
	  
	  logger = extent.startTest("Search valid ID number");
		login.SignIn(driver, sUsername, sPassword, AppURL);
		search.id_passport_AsylumNumber(driver, sIDNo, AppURL);
		FindID(sIDNo);
		logger.log(LogStatus.PASS, "search valid ID number executed successfully");
  }

	
  
  	public void FindID(String sIDNo) throws InterruptedException {
  		try
  		{
	  	results = "";
	  	results += "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();
	  
	  	System.out.println("Confirming HPID page");
	  	Thread.sleep(6000);
	  	driver.findElement(By.xpath("//*[@id='tabone']/table/tbody/tr[5]/td[contains(text(), '"+ sIDNo +"')]"));
	  	logger.log(LogStatus.PASS, "find ID number");
  		}
  		catch(Exception ex)
  		{
  			logger.log(LogStatus.FAIL, ex.getMessage());
  			Assert.fail();
  		}
	  
}
 
}