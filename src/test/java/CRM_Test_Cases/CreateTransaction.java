package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.Functions.Transactions;
import HelloGroup.CRM.TestData.Database;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CreateTransaction extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;
	
	
	public Search search = new Search();
	public Transactions transactions = new Transactions();
	public Database database = new Database();
	
	public CreateTransaction(){
		setId("2245");
	}
		
	
  @Test(priority=1, dataProvider = "transaction-test-data",dataProviderClass=Excel.class)
	public void RunTestCreateTransaction(String sHPID,String sUsername,String sPassword) throws InterruptedException, IOException{
	  
	  logger = extent.startTest("Create Transaction");
	  
	  
	  	PreparingData(sHPID);
	    login.SignIn(driver, sUsername, sPassword, appurl);
		search.hpId(driver, sHPID, appurl);
		search.ConfirmPageAfterSearch(driver, sHPID);
		CreateTrans(sHPID);
		EnterOTP(sHPID);
		
}
 
  
  
  

	public void PreparingData(String sHPID) throws InterruptedException, IOException {
		System.out.println("Updating previous transactions to 22");

		Thread.sleep(5000);

		//Changing previous transactions to update to 22 to prevent limits
		try {

			String query = "UPDATE HelloFin.HelloPaisaTransactions SET MatchStatus=22, transactiondate = '2016-08-22 00:00:00', PartnerStatus = 60 WHERE HPId = '" + sHPID
					+ "' and MatchStatus in ('20','21') ;";

			statement = connection.createStatement();
			statement.executeUpdate(query);
			
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}



  	public void CreateTrans(String sHPID)throws InterruptedException{
	  System.out.println("Creating a Transaction of last recipient created");
	  
      Reporter.log("STARTING TEST - Searching for last recipient created for   "+ sHPID +" ");
      System.out.println("No Data Returned");

	  
	  try {
          String query = "SELECT HPB.*, HPBS.*  FROM HelloFin.HelloPaisaBeneficiary HPB left outer join HelloFin.HelloPaisaTransactions HPT on  HPT.BeneficiaryId = HPB.Id inner join HelloFin.HelloPaisaBanks HPBS on HPB.BankId =  HPBS.id where HPB.HPID = '" + sHPID + "' and HPB.status = '1' and HPT.id is null and HPB.CreationDate > date_sub(now(), interval 60 minute) ORDER BY HPB.id DESC LIMIT 1;";

		  
		  statement = connection.createStatement();
          rs = statement.executeQuery(query);
          
          if (!rs.isBeforeFirst()) {
      	    System.out.println("No Data Returned");
      	    Reporter.log("No data found on the "+ query +" ");

             logger.log(LogStatus.FAIL, "Beneficiary does not exist\n Query "+query+" returned no data.");
      	    	Assert.fail();

      	    
      	  }
       
          results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();

          while(rs.next()){
        	  
        	  WebElement RecipientScreen = driver.findElement(By.id("btnRecipientView"));
              ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen); 
              Thread.sleep(5000);
              
              
        	  driver.findElement(By.id("btnRecipientView")).click();
              String ID = rs.getString("ID");
              Reporter.log("Creating transaction for Beneficary ID "+ ID +" ");
              String FirstName = rs.getString("FirstName");
              String LastName = rs.getString("Surname");
              String Bank = rs.getString("BankName");
              System.out.println("Bank is:" + Bank);
              

              driver.findElement(By.id(ID)).click();


              Reporter.log("Found last recipient created for   "+ sHPID +" with id "+ ID +" for "+ FirstName +" and bank "+ Bank +"  ");
              Thread.sleep(2000);
              //Clicking on
              driver.findElement(By.xpath("//*[@id='minAmount']")).click();
              Thread.sleep(2000);
              String TransactionAmmount = driver.findElement(By.xpath("//*[@id='transactionAmount']")).getText();
              String Totaltopay = driver.findElement(By.xpath("//*[@id='totalAmounttopay']")).getText();
              Reporter.log("On Clicking Minimun ammount the transaction ammount is: "+ TransactionAmmount +" and total to pay is: "+ Totaltopay +"  ");
              Thread.sleep(2000);
              
              driver.findElement(By.xpath("//*[@id='maxAmount']")).click();
              Thread.sleep(2000);
              String TransactionAmmountMax = driver.findElement(By.xpath("//*[@id='transactionAmount']")).getText();
              String TotaltopayMax = driver.findElement(By.xpath("//*[@id='totalAmounttopay']")).getText();
              Reporter.log("On Clicking Maximum ammount the transaction ammount is: "+ TransactionAmmountMax +" and total to pay is: "+ TotaltopayMax +"  ");
              
              
              driver.findElement(By.xpath("//*[@id='minAmount']")).click();
              
//              //Send Custom local Ammount
//              driver.findElement(By.xpath("//*[@id='LocalAmount']")).click();
//              driver.findElement(By.xpath("//*[@id='LocalAmount']")).clear();
//              driver.findElement(By.xpath("//*[@id='LocalAmount']")).sendKeys("1500");
//              Thread.sleep(2000);
//              //End//
              
              driver.findElement(By.xpath("//*[@id='BOPCode']/option[2]")).click();
              driver.findElement(By.xpath("//*[@id='addTransaction_rpt']")).click();
              
              String NameSurname = driver.findElement(By.xpath("//*[@id='benefId']")).getText();
              System.out.println("The Screen Says: "+NameSurname);
              Assert.assertTrue(NameSurname.contains(FirstName), "Name not visible");
     		  Assert.assertTrue(NameSurname.contains(LastName), "Surname not visible");
     		  
     		  String BankName = driver.findElement(By.xpath("//*[@id='BankDetails']")).getText();
     		  System.out.println("Bank is: "+BankName);
              //Assert.assertTrue(BankName.contains(Bank), "Bank Not visible");
              
              
              
              Thread.sleep(5000);
          }
      } catch (SQLException ex) {
    	  logger.log(LogStatus.FAIL, ex.getMessage());
         ex.printStackTrace();
         Assert.fail();
      }
	  
	 
  }
  
  	public void EnterOTP(String sHPID)throws InterruptedException{
	  System.out.println("Entering OTP to confirm transaction");
	  
	  try {
          String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '" + sHPID + "'";
          statement = connection.createStatement();
          rs = statement.executeQuery(query);
          
          if (!rs.isBeforeFirst()) {
      	    System.out.println("No Data Returned");
      	    Reporter.log("No data found on the "+ query +" ");
      	    Assert.fail();
      	  }
        
          
          results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();

          while(rs.next()){
        	  
        	  String Otp = rs.getString("Otp");
        	  
        	  WebElement RecipientScreen = driver.findElement(By.xpath("//*[@id='confirmtransactionCode']"));
              ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen); 
              
              Thread.sleep(5000);
              driver.findElement(By.xpath("//*[@id='confirmtransactionCode']")).click();
              Thread.sleep(5000);
              driver.findElement(By.xpath("//*[@id='confirmtransactionCode']")).sendKeys(Otp);
              Thread.sleep(5000);
              driver.findElement(By.xpath("//*[@id='submitConfirm_rtp']")).click();
              System.out.println("Confirming Transaction created successfully");
              Thread.sleep(2000);
              
              Boolean isPresent = driver.findElements(By.xpath("//*[@id='alertTT']/div")).size() > 0;
         	  System.out.println(isPresent);
         	
         	  if (isPresent == true) {
         			
         		String ActualMessage = driver.findElement(By.xpath("//*[@id='alertTT']/div")).getText(); 
                System.out.println(ActualMessage);
                Reporter.log("Entered OTP   "+ Otp +" and recieved the message "+ ActualMessage +" ");
                Assert.assertFalse(ActualMessage.contains("Transaction is greater than the remaining daily limit"), "Error daily limit does not match");
                Assert.assertFalse(ActualMessage.contains("You can only SEND CASH to 8 recipients for the month"), "Error 8 recipients does not match"); 
                //Assert.assertFalse(ActualMessage.contains("success"), "Error success does not match"); 
                Assert.assertFalse(ActualMessage.contains("This Offer is invalid"), "Error Offer is invalid does not match");
                
                results = "";
                results += "Actual Message is "+ ActualMessage +"" + "\n";

            	  } else {
                 
            	System.out.println("Error messages not found");

            	  }


              Thread.sleep(10000);
              driver.findElement(By.xpath("//*[@id='main']/div/div[1]/div[1]/h1/span[contains(text(), '"+ sHPID +"')]"));
             
              

              Thread.sleep(30000);
              try {
                  String query2 = "SELECT *  FROM HelloFin.HelloPaisaTransactions where HPID = '" + sHPID + "' and MatchStatus = '20' and PartnerStatus = '60' and TransactionDate > date_sub(now(), interval 5 minute) ORDER BY id DESC LIMIT 1;";

        		  statement = connection.createStatement();
                  rs = statement.executeQuery(query2);
                  
                  if (!rs.isBeforeFirst()) {
              	    System.out.println("No Data Returned");
              	    Reporter.log("No data found on the "+ query2 +" ");
              	    Assert.fail();
              	    logger.log(LogStatus.FAIL, "No transaction created");
              	  }
                  

                  results = "Method: " + new Object(){}.getClass().getEnclosingMethod().getName();

                  while(rs.next()){
                	  System.out.println("Confirming Transaction created successfully in DB");
                      String ReferenceNumber = rs.getString("RefNumber");
                      String Amount = rs.getString("LocalAmount");

                      Reporter.log("ENDING TEST - Created transaction referance   "+ ReferenceNumber +" for local ammount "+ Amount +" ");
                      
                      Thread.sleep(5000);
                  }
                  logger.log(LogStatus.PASS, "Transaction created successfully");
              } catch (SQLException ex) {
            	  logger.log(LogStatus.FAIL, ex.getMessage());
                 ex.printStackTrace();
                 Assert.fail();
              }
          }
      } catch (SQLException ex) {
    	  logger.log(LogStatus.FAIL, ex.getMessage());
         ex.printStackTrace();
         Assert.fail();
      }
  }

  
  	
}