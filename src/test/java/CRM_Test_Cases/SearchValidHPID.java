package CRM_Test_Cases;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class SearchValidHPID extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	
	public Search search = new Search();
	
	
	public SearchValidHPID(){
		setId("271");
	}
  

  
    @Test(priority=1, dataProvider = "HPID information",dataProviderClass=Excel.class)
  	public void RunTestSearchValidHPID(String sHPID,String sUsername,String sPassword, String AppURL) throws InterruptedException{
	  logger = extent.startTest("Search valid HP ID");
	  try
	  {
		login.SignIn(driver, sUsername, sPassword, AppURL);
		search.hpId(driver, sHPID, AppURL);
		search.ConfirmPageAfterSearch(driver, sHPID);
		logger.log(LogStatus.PASS, "Search valid HP ID executed successfully");
	  }
	  catch(Exception ex)
	  {
		  logger.log(LogStatus.FAIL, ex.getMessage());
	  }
  }


}