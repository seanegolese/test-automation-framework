package CRM_Test_Cases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class SearchValidCellNumber extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;

	public SearchValidCellNumber() {
		setId("269");
	}

	@Test(priority = 1, dataProvider = "cell number information",dataProviderClass=Excel.class)
	public void RunTestSearchValidHPID(String sMSISDN, String sUsername, String sPassword, String AppURL)
			throws InterruptedException {
		logger = extent.startTest("Search valid cell number");
		try {

			login.SignIn(driver, sUsername, sPassword, AppURL);
			MSISDNSearch(sMSISDN, AppURL);
			FindHPID(sMSISDN);
			logger.log(LogStatus.PASS, "Search valid cell number executed successfully");
		} catch (Exception ex) {
			logger.log(LogStatus.FATAL, ex.getMessage());
		}
	}


	public void MSISDNSearch(String sMSISDN, String AppURL) throws InterruptedException {

		Thread.sleep(15000);
		driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[2]/div/span[1]/label")).click();
		driver.findElement(By.xpath("//*[@id='cellphone']")).sendKeys(sMSISDN);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(200);

		// driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div/div[2]/div/form/div[1]/div/h4[contains(text(),
		// 'Success!')]"));
		String strPageURL = driver.getCurrentUrl();
		System.out.println("Page title: - " + strPageURL);

		results = "";
		results += "Actual: " + strPageURL + "\n";
		results += "Expected: " + "" + AppURL + "pages/main.php" + "\n";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		Assert.assertTrue(strPageURL.equalsIgnoreCase("" + AppURL + "pages/main.php"), "Page title doesn't match");
	}

	public void FindHPID(String sMSISDN) throws InterruptedException {
		results = "";
		results += "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		System.out.println("Confirming HPID page");
		Thread.sleep(6000);
		driver.findElement(By.xpath("//*[@id='tabone']/table/tbody/tr[10]/td[contains(text(), '" + sMSISDN + "')]"));
	}

}