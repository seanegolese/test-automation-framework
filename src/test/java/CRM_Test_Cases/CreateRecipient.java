package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CreateRecipient extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	public Search search = new Search();

	public CreateRecipient() {
		setId("2238");
	}

	@Test(priority = 1, dataProvider = "create-recipient-test-data",dataProviderClass=Excel.class)
	public void RunTestCreateRecipient(String sHPID, String sUsername, String sPassword, String Country,
			String PayoutPartner, String RecieptMethod, String RecieptFirstName, String RecieptLastName, String Number,
			String AccountNumber) throws InterruptedException, IOException {
	
		System.out.println(sHPID+" "+sUsername+" "+sPassword+" "+Country+" "+PayoutPartner+" "+RecieptMethod);
		
			logger = extent.startTest("Create Recipient");
		try {

			PreparingData(sHPID);
			login.SignIn(driver, sUsername, sPassword, appurl);
			search.hpId(driver, sHPID, appurl);
			search.ConfirmPageAfterSearch(driver, sHPID);
			CreatingRecipient(Country, RecieptMethod, PayoutPartner, RecieptFirstName, RecieptLastName, Number,
					AccountNumber);
			EnterValidOTP(sHPID);
			logger.log(LogStatus.PASS, "Recipient created successfully");
		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

	public void PreparingData(String sHPID) throws InterruptedException, IOException {
		System.out.println("Updating previous transactions to 22");

		Thread.sleep(5000);

		// Disabling previous beneficaries

		try {

			String query2 = "Update HelloFin.HelloPaisaBeneficiary set status = 5 where HpId = '" + sHPID + "'";

			statement = connection.createStatement();
			statement.executeUpdate(query2);
			logger.log(LogStatus.PASS, "Test data preparation");
		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, "Test data preparation failed\n" + ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}

	}

	public void CreatingRecipient(String Country, String RecieptMethod, String PayoutPartner, String RecieptFirstName,
			String RecieptLastName, String Number, String AccountNumber) throws InterruptedException {
		try {

			System.out.println("Creating Recipient");

			int num1, num2, num3; // 3 numbers in area code
			int set2, set3; // sequence 2 and 3 of the phone number

			Random generator = new Random();

			// Area code number; Will not print 8 or 9
			num1 = generator.nextInt(7) + 1; // add 1 so there is no 0 to begin
			num2 = generator.nextInt(8); // randomize to 8 because 0 counts as a
											// number in the generator
			num3 = generator.nextInt(8);
			set2 = generator.nextInt(643) + 100;
			set3 = generator.nextInt(8999) + 1000;

			String CellNumber = ("" + num1 + "" + num2 + "" + num3 + "" + set2 + "" + set3 + "");

			WebElement RecipientScreen = driver.findElement(By.xpath("//*[@id='RecipientsID']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", RecipientScreen);

			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id='RecipientsID']")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("btnRecipientCreate")).sendKeys(Keys.PAGE_DOWN);
			Thread.sleep(5000);
			driver.findElement(By.id("btnRecipientCreate")).click();
			Thread.sleep(5000);
			String ActualMessage = driver.findElement(By.xpath("//*[@id='CreateRecipient']/div")).getText();
			// System.out.println(ActualMessage);
			/// Reporter.log(ActualMessage);
			Assert.assertFalse(ActualMessage.contains("Customers are allowed a maximum of 8 recipients"),
					"More than 8 Recipients");

			/* Drop Down for Country */
			Thread.sleep(30000);
			Select selectByVisibleText = new Select(driver.findElement(By.id("drop_1")));
			selectByVisibleText.selectByVisibleText(Country);

			/* Drop Down for Collection Type */
			Thread.sleep(30000);
			Select selectByVisibleText1 = new Select(driver.findElement(By.id("drop_2")));
			selectByVisibleText1.selectByVisibleText(RecieptMethod);

			// System.out.println(PayoutPartner);
			/* Drop Down for PP */
			WebDriverWait wait = new WebDriverWait(driver, 20);

			// Wait until expected condition size of the dropdown increases and
			// becomes more than 1
			wait.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					Select select = new Select(driver.findElement(By.id("drop_4")));
					return select.getOptions().size() > 1;
				}
			});

			// To select the first option
			Select select = new Select(driver.findElement(By.id("drop_4")));
			select.selectByVisibleText(PayoutPartner);

			/// Thread.sleep(2000);
			driver.findElement(By.id("bName")).sendKeys(Keys.PAGE_DOWN);
			driver.findElement(By.id("bName")).clear();
			driver.findElement(By.id("bName")).sendKeys(RecieptFirstName);
			driver.findElement(By.id("bSurname")).clear();
			driver.findElement(By.id("bSurname")).sendKeys(RecieptLastName);
			driver.findElement(By.id("mobileNumbers")).clear();

			System.out.println("No: " + Number + "");
			// Sending Number
			if (Number.equalsIgnoreCase("n/a")) {

				driver.findElement(By.id("mobileNumbers")).sendKeys(CellNumber);

			} else if (Number != null) {

				driver.findElement(By.id("mobileNumbers")).sendKeys(Number);

			}

			Boolean isPresent = driver.findElements(By.xpath("//*[@id='BeneficiaryAccountNo']")).size() > 0;
			System.out.println("Found Account Number: " + isPresent);

			if (isPresent == true) {

				driver.findElement(By.xpath("//*[@id='BeneficiaryAccountNo']")).clear();
				driver.findElement(By.xpath("//*[@id='BeneficiaryAccountNo']")).sendKeys(AccountNumber);
				driver.findElement(By.id("add_rpt")).click();

			} else {

				driver.findElement(By.id("add_rpt")).click();

			}

			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id='alertC']/div[contains(text(), 'Verification Code has been sent')]"));

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

		} catch (Exception ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

	public void EnterValidOTP(String sHPID) throws InterruptedException {
		System.out.println("Verifying New OTP");
		try {
			String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
					+ sHPID + "'";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned " + query + "");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			} else {
				System.out.println(query + " Returned Data");
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				String Otp = rs.getString("Otp");
				System.out.println(Otp);

				Thread.sleep(20000);

				driver.findElement(By.id("confirmOTP")).sendKeys(Otp);
				Thread.sleep(20000);
				WebElement ConfirmButton = driver.findElement(By.id("confirm_rtp"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ConfirmButton);

				driver.findElement(By.id("confirm_rtp")).click();
				Thread.sleep(20000);

				Boolean isPresent = driver.findElements(By.xpath("//*[@id='alertC']/div")).size() > 0;
				System.out.println(isPresent);

				if (isPresent == true) {

					String ActualMessage = driver.findElement(By.xpath("//*[@id='alertC']/div")).getText();
					Reporter.log(ActualMessage);
					Assert.assertFalse(ActualMessage.contains("Duplicate account number"),
							"Error Duplicate account number Message does not match");
					Assert.assertFalse(ActualMessage.contains("Unable to create recipient"), "Terrorist");
					Assert.assertFalse(ActualMessage.contains("NOTICE: Please wait"), "Not Creating Recipient");
					results = "";
					results += "Actual Message is " + ActualMessage + "" + "\n";

				} else {
					System.out.println("Created Successfully");
					Reporter.log("Created Successfully");

				}

				Thread.sleep(5000);

			}

		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}

		try {

			// Thread.sleep(10000);
			String query2 = "SELECT * FROM HelloFin.HelloPaisaBeneficiary where HPid = '" + sHPID
					+ "' and CreationDate > date_sub(now(), interval 5 minute) and status = '1' ORDER BY id DESC LIMIT 1;";
			statement = connection.createStatement();
			rs = statement.executeQuery(query2);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query2 + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				String BenefID = rs.getString("Id");
				Reporter.log("Created BenefID: " + BenefID + " ");
				logger.log(LogStatus.PASS, "Created BenefID: " + BenefID);
			}

		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}
	}

}