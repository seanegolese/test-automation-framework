package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CreateTicketDash extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	public CreateTicketDash() {
		setId("2444");
	}

	
	@Test(priority = 1, dataProvider = "create-ticket-desh-test-data",dataProviderClass=Excel.class)
	public void RunTestCreateTicket(String sUsername, String sPassword, String FirstName, String LastName,
			String Province, String City, String Suburb, String Classification)
			throws InterruptedException, IOException {

		logger = extent.startTest("Create Ticket Desh");

		login.SignIn(driver, sUsername, sPassword, appurl);
		MyTickets();
		CreateTickets(FirstName, LastName, Province, City, Suburb, Classification);
		ConfirmingTicketCreation(sUsername);

	}

	public void MyTickets() throws InterruptedException {
		try
		{
		Thread.sleep(2000);
		driver.findElement(By.xpath("html/body/aside[1]/div/section/ul/li[1]/a/span[2]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='showInprogress']/h2/strong"));

		results = "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();
		
		logger.log(LogStatus.PASS, "My ticket");
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
		}
	}

	public void CreateTickets(String FirstName, String LastName, String Province, String City, String Suburb,
			String Classification) throws InterruptedException {

		
		System.out.println("Creating a ticket");
		Thread.sleep(3000);

		int num1, num3; // 3 numbers in area code
		int set2, set3; // sequence 2 and 3 of the phone number

		Random generator = new Random();

		// Area code number; Will not print 8 or 9
		num1 = generator.nextInt(7) + 1; // add 1 so there is no 0 to begin
		num3 = generator.nextInt(8);
		set2 = generator.nextInt(643) + 100;
		set3 = generator.nextInt(8999) + 1000;

		String CellNumber = ("0" + num1 + "" + num3 + "" + set2 + "" + set3 + "");
		System.out.println(CellNumber);

		driver.findElement(By.xpath("//*[@id='recipientsTransaction']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[2]/div/input"))
				.sendKeys(FirstName);
		driver.findElement(By.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[3]/div/input"))
				.sendKeys(LastName);
		driver.findElement(By.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[4]/div/input"))
				.sendKeys(CellNumber);

		/* Drop Down for Province */
		Thread.sleep(2000);
		Select selectByVisibleText = new Select(driver.findElement(
				By.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[5]/div/select")));
		selectByVisibleText.selectByVisibleText(Province);

		/* Drop Down for City */
		Thread.sleep(2000);
		Select selectByVisibleText1 = new Select(driver.findElement(By
				.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[6]/div[1]/span[1]/select")));
		selectByVisibleText1.selectByVisibleText(City);

		/* Drop Down for Suburb */
		Thread.sleep(2000);
		Select selectByVisibleText2 = new Select(driver.findElement(By
				.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[7]/div[1]/span[2]/select")));
		selectByVisibleText2.selectByVisibleText(Suburb);

		Thread.sleep(2000);
		driver.findElement(
				By.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[9]/div/input[2]")).click();

		/* Drop Down for Classification */
		Thread.sleep(2000);
		Select selectByVisibleText3 = new Select(driver.findElement(
				By.xpath("//*[@id='bs-logs-createTicket']/div/div/div[2]/div[2]/form/div/div[11]/div/select")));
		selectByVisibleText3.selectByVisibleText(Classification);

		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='save_ticket']")).click();


		Thread.sleep(5000);

		results = "";
		results += "Actual: " + "New ticket for : " + Classification + " Cell Number : " + CellNumber + " created"
				+ "\n";
		results = "Method: " + new Object() {
		}.getClass().getEnclosingMethod().getName();

		
		logger.log(LogStatus.PASS, "Ticket created");

		
	}

	public void ConfirmingTicketCreation(String sUsername) throws InterruptedException {
		System.out.println("Checking DB for Ticket Creation");

		try
		{
		
		Thread.sleep(30000);
		try {
			String query = "SELECT count(*) as Count FROM HelloFin.HelloCustomerLogging  CL inner join HelloFin.HelloAdmin HA on CL.CreatedBy = HA.HgAdminId where HA.email = '"
					+ sUsername
					+ "' and CreatedDate > date_sub(now(), interval 0.5 minute) and Status = '1' and CL.Classification = 'IVAN'";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {

				int Count = rs.getInt("Count");

				if (Count > 1) {
					System.out.println("DUPLICATE TICKETS");
					System.out.println(Count);
					Reporter.log("Duplicate Tickets");
					Assert.fail();
				}

				if (Count < 1) {
					System.out.println("NO DATA INSERTED");
					System.out.println(Count);
					Assert.fail();
				}

				results = "";
				results += "Count of tickets " + Count + " created" + "\n";
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			Assert.fail();
		}
		
		
		logger.log(LogStatus.PASS, "Ticket creation confirmed in database.");
		}
		catch(Exception ex)
		{
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
		
		
	}

}