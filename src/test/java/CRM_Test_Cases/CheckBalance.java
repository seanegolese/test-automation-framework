package CRM_Test_Cases;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CheckBalance extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	public Search search = new Search();

	public CheckBalance() {
		setId("2575");
	}

	@Test(priority = 1, dataProvider = "checkbalance-test-data", dataProviderClass = Excel.class)
	public void RunTestCheckBalance(String sHPID, String sUsername, String sPassword)
	

			throws InterruptedException, IOException {
		
		logger= extent.startTest("Check Balance");
		try
		{
		login.SignIn(driver, sUsername, sPassword, appurl);
		search.hpId(driver, sHPID, appurl);
		search.ConfirmPageAfterSearch(driver, sHPID);
		BalanceCheck(sHPID);
		TotalToPay(sHPID);
		logger.log(LogStatus.PASS, "Check balance executed successfully");
		
		}
		catch(Exception ex)
		{
		 logger.log(LogStatus.FAIL, ex.getMessage());	
		}
	}

	public void BalanceCheck(String sHPID) throws InterruptedException {
		System.out.println("Checking Balance");

		try {
			String query = "(select HelloFin.fnc_get_balance_for_hpid('" + sHPID
					+ "') - (SELECT HelloFin.fnc_get_outstanding_transaction_balance ('" + sHPID + "')) as balance)";

			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Assert.fail();
			}

			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				String balance = rs.getString("balance");

				DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("en", "UK")));

				BigDecimal value = new BigDecimal(balance);

				String Formatedbalance = (df.format(value.floatValue()));
				System.out.println(Formatedbalance);

				System.out.println("The Balance is :" + Formatedbalance + "");

				if (Formatedbalance.contains("-")) {
					System.out.println("Negative Balance should be 0");
					driver.findElement(
							By.xpath("//*[@id='main']/div/div[2]/div[5]/div/div/h2/span[contains(text(), '0.00')]"));
				} else {
					driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div[5]/div/div/h2/span[contains(text(), '"
							+ Formatedbalance + "')]"));
					String ActualBalance = driver
							.findElement(By.xpath("//*[@id='main']/div/div[2]/div[5]/div/div/h2/span")).getText();
					results = "";
					results += "Expected: " + "The Balance is : " + Formatedbalance + "" + "\n";
					results += "Actual: " + "The Actual Balance is : " + ActualBalance + "" + "\n";
					results += "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

					Reporter.log("The balance is : " + Formatedbalance + "");
				}

			}
			
			logger.log(LogStatus.PASS, "Balance checked successfully");
		} catch (SQLException ex) {
			
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}
	}

	public void TotalToPay(String sHPID) throws InterruptedException {
		System.out.println("Checking Total to Pay");

		try {

			String query = "SELECT HelloFin.fnc_get_outstanding_transaction_balance('" + sHPID + "') as totaltopay";

			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Assert.fail();
			}

			results += "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				String totaltopay = rs.getString("totaltopay");

				DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("en", "UK")));

				BigDecimal value = new BigDecimal(totaltopay);

				String FormatedTotaltoPay = (df.format(value.floatValue()));
				System.out.println(FormatedTotaltoPay);

				Thread.sleep(5000);
				driver.findElement(By.xpath("//*[@id='main']/div/div[2]/div[4]/div/div/h2/span[contains(text(), '"
						+ FormatedTotaltoPay + "')]"));
				String ActualTotalToPay = driver
						.findElement(By.xpath("//*[@id='main']/div/div[2]/div[4]/div/div/h2/span")).getText();

				Reporter.log("The total to pay is : " + FormatedTotaltoPay + "");

				results = "";
				results += "Expected: " + "The totaltopay is : " + totaltopay + "" + "\n";
				results += "Actual: " + "The Actual totaltopay is : " + ActualTotalToPay + "" + "\n";
				results += "Method: " + new Object() {
				}.getClass().getEnclosingMethod().getName();
			}
			logger.log(LogStatus.PASS, "Total to pay executed successfully");
		} catch (SQLException ex) {
			logger.log(LogStatus.FAIL, ex.getMessage());
			ex.printStackTrace();
			Assert.fail();
		}
	}

}