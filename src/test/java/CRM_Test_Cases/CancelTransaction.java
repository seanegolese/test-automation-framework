package CRM_Test_Cases;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import HelloGroup.CRM.Functions.Search;
import HelloGroup.CRM.Functions.Transactions;
import HelloGroup.CRM.POM.CRM_POM;
import HelloGroup.CRM.TestData.Database;
import HelloGroup.CRM.TestData.Excel;
import Utility.SeleniumBaseClass;

public class CancelTransaction extends SeleniumBaseClass {
	protected String results = "";
	protected String runId = "";
	protected boolean saveScreenshots = true;
	private static Statement statement;
	private static ResultSet rs;

	
	
	public Search search = new Search();
	public CRM_POM pageObject = new CRM_POM();
	public Transactions transactions = new Transactions();
	
	
	
	public CancelTransaction() {
		setId("64447");
	}

	
	@Test(priority = 1, dataProvider = "roles-test-data", dataProviderClass=Excel.class)
	public void RunTestSignIn(String sUsername, String sPassword,String parm1) throws InterruptedException, IOException {
		System.out.println("Test Case: "+ new Object() {}.getClass().getEnclosingMethod().getName());
		logger=extent.startTest("Login");
		login.SignIn(driver, sUsername, sPassword, appurl);
		
	}
	

	@Test(priority = 2, dataProvider = "mysql-data-provider",dataProviderClass=Database.class)	
	public void RunTestCancelTrans(String sHPID, String sTransID, String Ref) throws InterruptedException, IOException {
		System.out.println("Test Case: "+ this.getClass().getSimpleName().toString());
		logger = extent.startTest("Cancel Transaction");
		search.hpId(driver, sHPID, appurl);
		search.ConfirmPageAfterSearch(driver, sHPID);
		transactions.cancel(driver, sHPID, sTransID, Ref,statement);
		EnterOTP(sHPID, sTransID, Ref);	
	}
	
	
	
	public void EnterOTP(String sHPID, String TransID, String Ref) throws InterruptedException {
		Thread.sleep(20000);
		
		System.out.println("Entering OTP to confirm transaction");
		try {
			String query = "SELECT ho.otp FROM HelloFin.HelloCustomerOTP ho inner join HelloCustomer hc on ho.Msisdn = hc.Msisdn inner join HelloPaisaCustomer hp on hc.HgId = hp.HgId WHERE hp.HpId =  '"
					+ sHPID + "'";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);

			if (!rs.isBeforeFirst()) {
				System.out.println("No Data Returned");
				Reporter.log("No data found on the " + query + " ");
				Assert.fail();
			}

			results = "Method: " + new Object() {
			}.getClass().getEnclosingMethod().getName();

			while (rs.next()) {
				Thread.sleep(1000);
				String Otp = rs.getString("Otp");
				driver.findElement(By.xpath("//*[@id='confirmVerificationCodeCancel']")).click();
				Thread.sleep(1000);			
				driver.findElement(By.xpath("//*[@id='confirmVerificationCodeCancel']")).sendKeys(Otp);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='" + TransID + "']/div/div/div[2]/div[2]/button[1]")).click();
				

				Thread.sleep(1000);
				driver.findElement(
						By.xpath("//*[@id='main']/div/div[1]/div[1]/h1/span[contains(text(), '" + sHPID + "')]"));

				System.out.println("Confirming Cancel");

				
				try {
					String query2 = "SELECT *  FROM HelloFin.HelloPaisaTransactions where Refnumber = '" + Ref
							+ "' and MatchStatus = '22'  ORDER BY id DESC LIMIT 1;";

					statement = connection.createStatement();
					rs = statement.executeQuery(query2);

					if (!rs.isBeforeFirst()) {
						System.out.println("No Data Returned");
						Reporter.log("No data found on the " + query2 + " ");
						Assert.fail();
					}

					results = "Method: " + new Object() {
					}.getClass().getEnclosingMethod().getName();

					while (rs.next()) {
						String ReferenceNumber = rs.getString("RefNumber");
						String Amount = rs.getString("LocalAmount");

						Reporter.log("ENDING TEST - Canceled transaction referance   " + ReferenceNumber
								+ " for local ammount " + Amount + " ");

						
						logger.log(LogStatus.PASS, "Transaction cancelled successfully");
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.log(LogStatus.FAIL, ex.getMessage());
					Assert.fail();
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			logger.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail();
		}
	}

}